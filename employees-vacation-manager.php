<?php
/*
   Plugin Name: Employees Vacation Manager
   Plugin URI: http://wordpress.org/extend/plugins/employees-vacation-manager/
   Version: 0.1
   Author: Aries Prodesign. Eduardo Garces Hernandez
   Description: Manage your employees
   Text Domain: employees-vacation-manager
   License: GPLv3
*/

/**
 * Employees Vacation Manager Extension
 *
 * @category     Extension
 * @copyright    Copyright © 2016 Aries Prodesign (http://ariesprodesign.de/)
 * @author       Aries Prodesign. Eduardo Garces Hernandez
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version     Release: 1.0.0
 */

$EmployeesManager_minimalRequiredPhpVersion = '5.0';

/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function EmployeesManager_noticePhpVersionWrong() {
    global $EmployeesManager_minimalRequiredPhpVersion;
    echo '<div class="updated fade">' .
      __('Error: plugin "Employees Manager" requires a newer version of PHP to be running.',  'employees-vacation-manager').
            '<br/>' . __('Minimal version of PHP required: ', 'employees-vacation-manager') . '<strong>' . $EmployeesManager_minimalRequiredPhpVersion . '</strong>' .
            '<br/>' . __('Your server\'s PHP version: ', 'employees-vacation-manager') . '<strong>' . phpversion() . '</strong>' .
         '</div>';
}


function EmployeesManager_PhpVersionCheck() {
    global $EmployeesManager_minimalRequiredPhpVersion;
    if (version_compare(phpversion(), $EmployeesManager_minimalRequiredPhpVersion) < 0) {
        add_action('admin_notices', 'EmployeesManager_noticePhpVersionWrong');
        return false;
    }
    return true;
}


/**
 * Initialize internationalization (i18n) for this plugin.
 * References:
 *      http://codex.wordpress.org/I18n_for_WordPress_Developers
 *      http://www.wdmac.com/how-to-create-a-po-language-translation#more-631
 * @return void
 */
function EmployeesManager_i18n_init() {
    $pluginDir = dirname(plugin_basename(__FILE__));
    load_plugin_textdomain('employees-vacation-manager', false, $pluginDir . '/languages/');
}


//////////////////////////////////
// Run initialization
/////////////////////////////////

// Initialize i18n
add_action('plugins_loaded','EmployeesManager_i18n_init');

// Run the version check.
// If it is successful, continue with initialization for this plugin
if (EmployeesManager_PhpVersionCheck()) {
    // Only load and run the init function if we know PHP version can parse it
    include_once('employees-vacation-manager_init.php');
    EmployeesManager_init(__FILE__);
}

function add_query_vars_filter( $vars ){
    $vars[] = "em-page";
    return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );


function custom_rewrite_tag() {
    add_rewrite_tag('%em-page%', '([^&]+)');
    add_rewrite_rule('^([^/]+)/em-page/([0-9]+)/?', 'index.php?pagename=$matches[1]&em-page=$matches[2]', 'top');
}

add_action('init', 'custom_rewrite_tag');


require_once('EmployeesManager_Pagetemplater.php');
