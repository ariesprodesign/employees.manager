<?php
include_once('../EmployeesManager_Utils.php');

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$raw_form_key = $userdata->user_login.$userdata->user_email.$userdata->ID;
$form_key = EmployeesManager_Utils::encrypt_decrypt('encrypt', $raw_form_key, $userdata->user_pass);
$placeholder = str_replace(array('YYYY', 'MM','DD'), array('2016', '01','01'), $date_format).' - ' .str_replace(array('YYYY', 'MM','DD'), array('2016', '02','01'), $date_format);
?>
<div class="em-form">
    <form action="" method="post">
        <input name="form-key" type="hidden" value="<?php echo $form_key ?>">
        <div class="em-field-label"><label for="vacation-type"><?php  _e('Vacation period', 'employees-vacation-manager')  ?> *</label></div>
        <div class="em-field-area">
            <input type="text" name="date-range" id="date-range" placeholder="<?php echo $placeholder ?>" class="em-form-field" maxlength="23" >
        </div>
        <br>
        <div class="em-field-label"><label for="vacation-type"><?php  _e('Vacation type', 'employees-vacation-manager')  ?> *</label></div>
        <div class="em-field-area">
            <select name="vacation-type" id="vacation-type">
                <option value="">--<?php _e('Please choose', 'employees-vacation-manager')?>--</option>
                <option value="paid-vacation"><?php  _e('Paid vacation', 'employees-vacation-manager')  ?></option>
                <option value="unpaid-vacation"><?php  _e('Unpaid vacation', 'employees-vacation-manager')  ?></option>
            </select>
        </div>
        <br/>
        <div class="em-field-label"><label for="vacation-comment"><?php  _e('Comment', 'employees-vacation-manager')  ?></label></div>
        <div class="em-field-area">
            <input type="text" name="vacation-comment" id="vacation-comment" maxlength="100"  placeholder="<?php  _e('Comment', 'employees-vacation-manager')  ?>" class="em-form-field">
        </div>
        <br/>
        <div class="notice">(* = )<?php  _e('Mandatory', 'employees-vacation-manager')  ?></div>
        <br><br>
        <input type="submit" class="em-button" value="<?php _e('Send request', 'employees-vacation-manager') ?>" id="em_account_submit" name="um_account_submit">
    </form>
</div>
