<?php

include_once('../EmployeesManager_Utils.php');

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wpdb;
$table_name = $wpdb->prefix .  'em_vacation_request';
$vacations_days = (get_option('em_available_paid_vacation'))? get_option('em_available_paid_vacation'): 30;
$statuses = array(0 => __('In Progress', 'employees-vacation-manager'), 1 => __('Approved', 'employees-vacation-manager'), 2 => __('Rejected', 'employees-vacation-manager') );
$date_format = (get_option('em_date_format'))? get_option('em_date_format'): 'DD.MM.YYYY';
$userdata = wp_get_current_user();
$notification = apply_filters('employees-vacation-manager-notification', array());
$vacation_requests = $wpdb->get_results( "SELECT * FROM ".$table_name. " WHERE user=".$userdata->ID. " ORDER BY time DESC", ARRAY_A );

EmployeesManager_Utils::enqueue_frontend_scripts_and_styles();

get_header();
?>
	<?php do_action( 'employees_manager_before_main_content' );	?>
    <div class="em content">
		<?php while ( have_posts() ) : the_post(); ?>
            <?php the_content() ?>
        <?php endwhile; // end of the loop. ?>

        <div class="wrap">
            <?php if(isset($notification['type'])): ?>
            <p class="em-notice <?php echo $notification['type']?>"><?php echo $notification['text'];?></p>
            <?php endif ?>
            <h2><?php echo ($title = get_option('em_user_page_title'))? $title : __('Vacation requests Overview', 'employees-vacation-manager');?></h2>
            <div class="em-nav-item">
                <a  href="#form-add-vacation-request" class="button-primary" id="add-new-request"><?php  _e('Add a new request', 'employees-vacation-manager')  ?></a>
            </div>
            <?php do_action('em_user_vacation_requests_table', $vacation_requests) ?>

            <div class="info-table">
                <?php do_action('em_info_table', $userdata->ID);?>
            </div>
            <div style="display:none">
                <div id="form-add-vacation-request" style="display: none">
                    <h2><?php _e('Add a new request', 'employees-vacation-manager');?></h2>
                    <?php include 'vacation-request-form.php'; ?>
                </div>
            </div>
        </div>

	<?php do_action( 'employees_manager_after_main_content' ); ?>
    <script type="text/javascript">
        (function($){
            $(document).ready(function() {
                $("#add-new-request").fancybox({
                    maxWidth	: 400,
                    maxHeight	: 500,
                    fitToView	: false,
                    width		: '70%',
                    height		: '70%',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'none',
                    closeEffect	: 'none'
                });

                var configObject = {
                    format: '<?php echo $date_format ?>',
                    language: 'de',
                    separator: ' - ',
                    'maxDays' : <?php echo $vacations_days?>,
                    'minDays' : 1
                }
                $('#date-range').dateRangePicker(configObject);

                setTimeout(function(){
                    if($('.em-notice').length > 0)
                        $('.em-notice').slideUp(300);
                }, 10000);
            });
        })(jQuery)

    </script>
</div>
<?php do_action( 'employees_manager_sidebar' );	?>
<?php get_footer(); ?>
