<?php
/**
 * Employees Vacation Manager Extension
 *
 * @category     Extension
 * @copyright    Copyright © 2016 Aries Prodesign (http://ariesprodesign.de/)
 * @author       Aries Prodesign. Eduardo Garces Hernandez
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version     Release: 1.0.0
 */


include_once('EmployeesManager_InstallIndicator.php');
include_once('EmployeesManager_ListTable.php');
include_once('EmployeesManager_UsersListTable.php');
include_once('EmployeesManager_HolidaysListTable.php');
include_once('EmployeesManager_Utils.php');

class EmployeesManager_AdminPages extends  EmployeesManager_InstallIndicator{



    /**
     * @return void
     */
    public function settingsPage() {
        if (!current_user_can('manage_options')) {
            ?>
            <div class="wrap"><?php _e('You do not have sufficient permissions to access this page.', 'employees-vacation-manager');?></div>
            <?php
        }else{
            $optionMetaData = $this->getOptionMetaData();

            // Save Posted Options
            if ($optionMetaData != null) {
                foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                    if (isset($_POST[$aOptionKey])) {
                        $this->updateOption($aOptionKey, $_POST[$aOptionKey]);
                    }
                }
            }

            // HTML for the page
            $settingsGroup = get_class($this) . '-settings-group';
            ?>
            <div class="wrap">
                <h2><?php _e('Plugin Settings', 'employees-vacation-manager'); ?></h2>
                <table cellspacing="1" cellpadding="2"><tbody>
                    <tr><td><?php _e('PHP Version', 'employees-vacation-manager'); ?></td>
                        <td><?php echo phpversion(); ?>
                            <?php
                            if (version_compare('5.2', phpversion()) > 0) {
                                echo '&nbsp;&nbsp;&nbsp;<span style="background-color: #ffcc00;">';
                                _e('(WARNING: This plugin may not work properly with versions earlier than PHP 5.2)', 'employees-vacation-manager');
                                echo '</span>';
                            }
                            ?>
                        </td>
                    </tr>
                    <tr><td><?php _e('MySQL Version', 'employees-vacation-manager'); ?></td>
                        <td><?php echo $this->getMySqlVersion() ?>
                            <?php
                            echo '&nbsp;&nbsp;&nbsp;<span style="background-color: #ffcc00;">';
                            if (version_compare('5.0', $this->getMySqlVersion()) > 0) {
                                _e('(WARNING: This plugin may not work properly with versions earlier than MySQL 5.0)', 'employees-vacation-manager');
                            }
                            echo '</span>';
                            ?>
                        </td>
                    </tr>
                    </tbody></table>

                <h2><?php  _e($this->getPluginDisplayName(), 'employees-vacation-manager'); echo ' '; _e('Settings', 'employees-vacation-manager'); ?></h2>

                <form method="post" action="" autocomplete="off">
                    <?php settings_fields($settingsGroup); ?>
                    <style type="text/css">
                        table.plugin-options-table {width: 100%; padding: 0;}
                        table.plugin-options-table tr:nth-child(even) {background: #f9f9f9}
                        table.plugin-options-table tr:nth-child(odd) {background: #FFF}
                        table.plugin-options-table tr:first-child {width: 35%;}
                        table.plugin-options-table td {vertical-align: middle;}
                        table.plugin-options-table td+td {width: auto}
                        table.plugin-options-table td > p {margin-top: 0; margin-bottom: 0;}
                    </style>
                    <table class="plugin-options-table"><tbody>
                        <?php
                        if ($optionMetaData != null) {
                            foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                                $displayText = $aOptionMeta[0];
                                ?>
                                <tr valign="top">
                                    <th scope="row"><p><label for="<?php echo $aOptionKey ?>"><?php echo $displayText ?></label></p></th>
                                    <td>
                                        <?php $this->createFormControl($aOptionKey, $aOptionMeta, $this->getOption($aOptionKey, "")); ?>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody></table>
                    <p class="submit">
                        <input type="submit" class="button-primary"
                               value="<?php _e('Save Changes', 'employees-vacation-manager') ?>"/>
                    </p>
                </form>
            </div>
        <?php

        }

    }

    /**
     * @return void
     */
    public function vacationsOverviewPage() {
        global $wpdb;
        $users = get_users();
        $date_format = $this->getOption('date_format', 'DD.MM.YYYY');
        $table_name = $this->prefixTableName('vacation_request');
        $php_date_format = $this->getPhpDateFomat();
        $placeholder = str_replace(array('YYYY', 'MM','DD'), array('2016', '01','01'), $date_format).' - ' .str_replace(array('YYYY', 'MM','DD'), array('2016', '02','01'), $date_format);
        $userdata = wp_get_current_user();
        $raw_form_key = $userdata->user_login.$userdata->user_email.$userdata->ID;
        $form_key = EmployeesManager_Utils::encrypt_decrypt('encrypt', $raw_form_key, $userdata->user_pass);

        if (!$this->canUserAuthorizeVacation()) {
            ?>
            <div class="wrap"><?php _e('You do not have sufficient permissions to access this page.', 'employees-vacation-manager');?></div>
            <?php
        }else{
            if(isset($_GET['showvacationrequest'])){
                do_action('em_show_vacation_request_details', $_GET['showvacationrequest']);
            }else{

                if(isset($_POST['admin-date-range']) && !empty($_POST['admin-date-range'])){

                    $decrypt_form_key = EmployeesManager_Utils::encrypt_decrypt("decrypt", $_POST['admin-form-key'], $userdata->user_pass);
                    if ($decrypt_form_key != $raw_form_key) {
                        add_filter( 'admin-employees-vacation-manager-notification', function(){
                            return array("type" => "err", "text" => __('permission denied', "employees-vacation-manager"));
                        });
                    }else{
                        $this->saveAdminVacationRequest();
                    }
                }

                $current = "all";
                $where = "";

                if(isset($_GET) && isset($_GET['status'])){
                    $where = " WHERE status=". $_GET['status'];
                    switch($_GET['status']){
                        case 0: $current = "in-progress"; break;
                        case 1: $current = "approved"; break;
                        case 2: $current = "rejected"; break;
                    }
                }

                if(isset($_GET['em-v-type']) && $_GET['em-v-type'] != "-1"){
                    $where .= ($where == "") ? " WHERE type='". $_GET['em-v-type']."'" : " AND type='". $_POST['em-v-type']."'";
                }
                if(isset($_GET['em-f-user']) && $_GET['em-f-user'] != "-1"){
                    $where .= ($where == "") ? " WHERE user=". $_GET['em-f-user'] : " AND user=". $_GET['em-f-user'];
                }
                if(isset($_GET['em-dr-start']) && $_GET['em-dr-start'] != ""){
                    $dates = explode(' - ', $_GET['em-dr-start']);
                    if(count($dates) == 2){
                        $fromDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[0]));
                        $toDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[1]));
                        $compare = " from_date >='".  date('Y-m-d', strtotime($fromDate->format('Y-m-d'))) . "' AND from_date <='". date('Y-m-d', strtotime($toDate->format('Y-m-d'))) ."'" ;
                        $where .= ($where == "") ? " WHERE ".$compare : " AND ".$compare;
                    }
                }
                if(isset($_GET['em-dr-end']) && $_GET['em-dr-end'] != ""){
                    $dates = explode(' - ', $_GET['em-dr-end']);
                    if(count($dates) == 2){
                        $fromDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[0]));
                        $toDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[1]));
                        $compare = " to_date >='".  date('Y-m-d', strtotime($fromDate->format('Y-m-d'))) . "' AND to_date <='". date('Y-m-d', strtotime($toDate->format('Y-m-d'))) ."'" ;
                        $where .= ($where == "") ? " WHERE ".$compare : " AND ".$compare;
                    }
                }

                $vacation_requests = $wpdb->get_results( "SELECT * FROM ".$table_name.$where );
                $notification = apply_filters('admin-employees-vacation-manager-notification', array());

                ?>

                <?php if(isset($notification['type'])): ?>
                    <p class="em-notice <?php echo $notification['type']?>"><?php echo $notification['text'];?></p>
                <?php endif ?>
                <div class="wrap">
                    <h2><?php _e('Vacation requests Overview', 'employees-vacation-manager'); ?></h2>
                    <br/>
                    <a  href="#add-user-vacations-request-form" class="button-primary" id="add-new-request"><?php  _e('Created a new vacation request', 'employees-vacation-manager')  ?></a>
                    <br/><br/>
                    <?php $this->getFilterLinks($current); ?>
                    <br class="clear">
                    <?php do_action('em_vacation_request_filter_form'); ?>
                    <form method="post" action="">
                        <?php
                        $vacationRequestListTable = new EmployeesManager_ListTable($vacation_requests);
                        $vacationRequestListTable->prepare_items();
                        $vacationRequestListTable->display();
                        ?>
                    </form>
                    <div id="add-user-vacations-request-form" style="display:none;" >
                        <div class="em-form">
                            <form action="" method="post" autocomplete="off">
                                <input name="admin-form-key" type="hidden" value="<?php echo $form_key ?>">
                                <div class="em-field-label"><label for="admin-user"><?php  _e('User', 'employees-vacation-manager')  ?> *</label></div>
                                <div class="em-field-area">
                                    <select name="admin-user" id="admin-user">
                                        <option value="">--<?php _e('Please choose', 'employees-vacation-manager')?>--</option>
                                        <?php foreach($users as $user_item): ?>
                                            <?php $user_name = ($user_item->last_name != "")?$user_item->last_name . ', ' . $user_item->first_name:$user_item->display_name?>
                                            <option value="<?php echo $user_item->ID ?>"><?php echo  $user_name?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="em-field-label"><label for="admin-vacation-type"><?php  _e('Vacation period', 'employees-vacation-manager')  ?> *</label></div>
                                <div class="em-field-area">
                                    <input type="text" name="admin-date-range" id="admin-date-range" placeholder="<?php echo $placeholder ?>" class="em-form-field">
                                </div>
                                <br>
                                <div class="em-field-label"><label for="admin-vacation-type"><?php  _e('Vacation type', 'employees-vacation-manager')  ?> *</label></div>
                                <div class="em-field-area">
                                    <select name="admin-vacation-type" id="admin-vacation-type">
                                        <option value="">--<?php _e('Please choose', 'employees-vacation-manager')?>--</option>
                                        <option value="paid-vacation"><?php  _e('Paid vacation', 'employees-vacation-manager')  ?></option>
                                        <option value="unpaid-vacation"><?php  _e('Unpaid vacation', 'employees-vacation-manager')  ?></option>
                                    </select>
                                </div>
                                <br>
                                <div class="em-field-label"><label for="admin-vacation-comment"><?php  _e('Comment', 'employees-vacation-manager')  ?></label></div>
                                <div class="em-field-area">
                                    <input type="text" name="admin-vacation-comment" id="admin-vacation-comment" placeholder="<?php  _e('Comment', 'employees-vacation-manager')  ?>" class="em-form-field">
                                </div>
                                <br/>
                                <div class="em-field-label em-horizontal-area"><label for="admin-approve"><?php  _e('Approve', 'employees-vacation-manager')  ?>?</label></div>
                                <div class="em-field-area em-horizontal-area">
                                    <input type="checkbox" checked="checked" name="admin-approve" id="admin-approve" value="1">
                                </div>
                                <br class="clear">
                                <div>(* = <?php  _e('Mandatory', 'employees-vacation-manager')  ?>)</div>
                                <br>
                                <input type="submit" class="em-button" value="<?php _e('Send request', 'employees-vacation-manager') ?>" id="em_account_submit" name="um_account_submit">
                            </form>
                        </div>
                    </div>
                    <script type="text/javascript">
                        (function($){
                            $(document).ready(function() {

                                $("#add-new-request").fancybox({
                                    maxWidth	: 500,
                                    maxHeight	: 500,
                                    fitToView	: false,
                                    width		: '70%',
                                    height		: '70%',
                                    autoSize	: false,
                                    closeClick	: false,
                                    openEffect	: 'none',
                                    closeEffect	: 'none'
                                });

                                var configObject = {
                                    format: '<?php echo $date_format ?>',
                                    language: '<?php echo __('en', 'employees-vacation-manager') ?>',
                                    separator: ' - ',
                                    'minDays' : 1
                                }
                                $('#admin-date-range').dateRangePicker(configObject);
                                $('#admin-filter-date-range-start').dateRangePicker(configObject);
                                $('#admin-filter-date-range-end').dateRangePicker(configObject);

                                $('.button.action').on('click', function(){
                                    var action = $('#bulk-action-selector-bottom').val();
                                    if(action == 'bulk-delete'){
                                        var resp = confirm("<?php _e('Are you sure that you want to remove this entry?', 'employees-vacation-manager') ?>");
                                        return (resp)? true : false;
                                    }
                                    return true;
                                });

                                setTimeout(function(){
                                    if($('.em-notice').length > 0)
                                        $('.em-notice').slideUp(300);
                                }, 10000);

                            });
                        })(jQuery)

                    </script>
                </div>
            <?php
            }
        }
    }

    /**
     * @return void
     */
    public function employeesOverviewPage() {

        if(isset($notification['type'])): ?>
            <p class="em-notice <?php echo $notification['type']?>"><?php echo $notification['text'];?></p>
        <?php endif ?>
        <div class="wrap">
            <h2><?php _e('Employees Overview', 'employees-vacation-manager'); ?></h2>
            <?php
            $usersListTable = new EmployeesManager_UsersListTable();
            $usersListTable->prepare_items();
            $usersListTable->display();
            ?>
        </div>
    <?php

    }


    public function  calenderOverviewPage(){
        if(is_admin()){
            EmployeesManager_Utils::enqueue_calender_scripts_and_styles();
        }

        global $wpdb;
        $table_name = $this->prefixTableName('vacation_request');
        $current_year = date('Y');
        $first_year = (int)$current_year - 1;
        $last_year = (int)$current_year + 1;
        $fromTempDate =  DateTime::createFromFormat('Y-m-d', $first_year.'-01-01');
        $toTempDate =  DateTime::createFromFormat('Y-m-d', $last_year.'-12-31');
        $vacation_requests = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE status=1 AND from_date >='".$fromTempDate->format('Y-m-d'). "'  AND to_date <='".$toTempDate->format('Y-m-d'). "'" );
        $count = 0;
        ?>
        <h1><?php _e('Calender Overview', 'employees-vacation-manager'); ?></h1>
        <?php if(is_admin()):?>
        <p class="em-notice warning"><?php _e('Create a new page and insert the shortcode [ShowVacationCalender] to show the calender on the frontend', 'employees-vacation-manager'); ?></p>
        <?php endif ?>
        <div class="calender-wrap">
            <div id='calendar'></div>
        </div>
        <script type="text/javascript">
            (function($){
                $(document).ready(function() {

                    $('#calendar').fullCalendar({
                        events: [
                            <?php foreach($vacation_requests as $vacation_request):
                            $user_info = get_userdata($vacation_request->user);
                            $from_date = date_create($vacation_request->from_date);
                            $to_date = date_create($vacation_request->to_date);
                            ?>
                            {
                                title  : '<?php echo $user_info->last_name .  ", " . $user_info->first_name  ?>',
                                start  : '<?php echo date_format($from_date, 'Y-m-d')?>'    ,
                                end    : '<?php echo date_format($to_date, 'Y-m-d')?>'
                            }
                            <?php if(count($vacation_requests) != $count) echo "," ?>
                            <?php $count++ ?>
                            <?php endforeach ?>
                        ]
                    });
                });
            })(jQuery)

        </script>
        <?php
    }

    public function  holidaysOverviewPage(){

        if(is_admin()){
            EmployeesManager_Utils::enqueue_holidays_scripts_and_styles();
        }

        $userdata = wp_get_current_user();
        $raw_form_key = $userdata->user_login.$userdata->user_email.$userdata->ID;
        $form_key = EmployeesManager_Utils::encrypt_decrypt('encrypt', $raw_form_key, $userdata->user_pass);
        $notification = apply_filters('admin-holidays-vacation-manager-notification', array());
        $date_format = $this->getOption('date_format', 'DD.MM.YYYY');
        $js_date_formats = $this->getJavaScriptDateFomat();
        $placeholder = str_replace(array('YYYY', 'MM','DD'), array('2016', '01','01'), $date_format);
        ?>

        <?php if(isset($notification['type'])): ?>
            <p class="em-notice <?php echo $notification['type']?>"><?php echo $notification['text'];?></p>
        <?php endif ?>
        <div class="wrap">
        <h2><?php _e('Holidays Overview', 'employees-vacation-manager'); ?></h2>
        <br/>
        <a  href="#add-holiday-form" class="button-primary" id="add-new-holiday"><?php  _e('Add a new holiday', 'employees-vacation-manager')  ?></a>

        <br class="clear">

        <form method="post" action="">
            <?php
            $holidaysListTable = new EmployeesManager_HolidaysListTable();
            $holidaysListTable->prepare_items();
            $holidaysListTable->display();
            ?>
        </form>

        <div id="add-holiday-form" style="display:none;" >
            <div class="em-form">
                <h2><?php _e('Add a new holiday', 'employees-vacation-manager');?></h2>
                <form action="" method="post" autocomplete="off">
                    <input name="admin-form-key" type="hidden" value="<?php echo $form_key ?>">
                    <div class="em-field-label"><label for="holiday-date"><?php  _e('Date', 'employees-vacation-manager')  ?> *</label></div>
                    <div class="em-field-area">
                        <input type="text" name="holiday-date" id="holiday-date"  class="em-form-field datepicker" placeholder="<?php echo $placeholder ?>">
                    </div>
                    <div class="em-field-label"><label for="holiday-comment"><?php  _e('Comment', 'employees-vacation-manager')  ?></label></div>
                    <div class="em-field-area">
                        <input type="text" name="holiday-comment" id="holiday-comment" placeholder="<?php  _e('Comment', 'employees-vacation-manager')  ?>" class="em-form-field">
                    </div>
                    <br class="clear">
                    <div>(* = <?php  _e('Mandatory', 'employees-vacation-manager')  ?>)</div>
                    <br>
                    <input type="submit" class="em-button" value="<?php _e('Add holiday', 'employees-vacation-manager') ?>" id="em_holidays_submit" name="em_holidays_submit">
                </form>
            </div>
        </div>
        <script type="text/javascript">
            (function($){
                $(document).ready(function() {

                    $("#add-new-holiday").fancybox({
                        maxWidth	: 500,
                        maxHeight	: 500,
                        fitToView	: false,
                        width		: '70%',
                        height		: '70%',
                        autoSize	: false,
                        closeClick	: false,
                        openEffect	: 'none',
                        closeEffect	: 'none'
                    });


                    $('.button.action').on('click', function(){
                        var action = $('#bulk-action-selector-bottom').val();
                        if(action == 'bulk-delete'){
                            var resp = confirm("<?php _e('Are you sure that you want to remove this entry?', 'employees-vacation-manager') ?>");
                            return (resp)? true : false;
                        }
                        return true;
                    });

                    setTimeout(function(){
                        if($('.em-notice').length > 0)
                            $('.em-notice').slideUp(300);
                    }, 10000);

                    jQuery('.datepicker').datepicker({ dateFormat: '<?php echo $js_date_formats[$date_format] ?>' });

                });
            })(jQuery)

        </script>
        </div>
        <?php

    }


} 