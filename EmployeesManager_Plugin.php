<?php


include_once('EmployeesManager_LifeCycle.php');
include_once('EmployeesManager_Utils.php');

class EmployeesManager_Plugin extends EmployeesManager_LifeCycle {

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31

        $roles = array();
        $dateFormat = array('DD.MM.YYYY' =>'DD.MM.YYYY' , 'DD-MM-YYYY'  => 'DD-MM-YYYY', 'MM.DD.YYYY'  => 'MM.DD.YYYY', 'MM-DD-YYYY'  => 'MM-DD-YYYY',
                            'YYYY.MM.DD'  => 'YYYY.MM.DD', 'YYYY-MM-DD'  => 'YYYY-MM-DD');

        foreach($this->getEditableRoles() as $key => $role){
            $roles[$key] = __($role ["name"], 'employees-vacation-manager');
        }
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            'user_page_title' => array(__('Enter user page title', 'employees-vacation-manager'), 'text'),
            'available_paid_vacation' => array(__('Amount of available paid vacation days per year', 'employees-vacation-manager'), 'text'),
            'can_do_authorize_vacation' => array(__('Who can authorize the vacation days?', 'employees-vacation-manager'), 'multiple',$roles),
            'can_do_request_vacation' => array(__('Who can request vacations?', 'employees-vacation-manager'), 'multiple',$roles),
            'date_format' => array(__('Date format', 'employees-vacation-manager'), 'select', $dateFormat),
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

    protected function initOptions() {
        /*$options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }*/
    }

    public function getPluginDisplayName() {
        return 'Employees Vacation';
    }

    protected function getMainPluginFileName() {
        return 'employees-vacation-manager.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        global $wpdb;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $charset_collate = $wpdb->get_charset_collate();

        $table_name = $this->prefixTableName('vacation_request');
        $sql = "CREATE TABLE IF NOT EXISTS ". $table_name ." (
                  id mediumint(9) NOT NULL AUTO_INCREMENT,
                  time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                  from_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                  to_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                  type tinytext NOT NULL,
                  user int NOT NULL,
                  comment tinytext NULL,
                  status int DEFAULT '0' NOT NULL,
                  history longtext NULL,
                  UNIQUE KEY id (id)
                ) $charset_collate;";

        dbDelta( $sql );

        $table_name = $this->prefixTableName('vacation_request_dates');
        $sql = "CREATE TABLE IF NOT EXISTS ". $table_name ." (
                  id mediumint(9) NOT NULL AUTO_INCREMENT,
                  request_id int NOT NULL,
                  user int NOT NULL,
                  from_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                  to_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                  year int NOT NULL,
                  days int NOT NULL,
                  working_days int NOT NULL,
                  UNIQUE KEY id (id)
                ) $charset_collate;";

        dbDelta( $sql );

        $table_name = $this->prefixTableName('holidays_dates');
        $sql = "CREATE TABLE IF NOT EXISTS ". $table_name ." (
                  id mediumint(9) NOT NULL AUTO_INCREMENT,
                  date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                  comment tinytext NULL,
                  UNIQUE KEY id (id)
                ) $charset_collate;";

        dbDelta( $sql );
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        global $wpdb;
        $tableName = $this->prefixTableName('vacation_request');
        $wpdb->query("DROP TABLE IF EXISTS ".$tableName);
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade() {
    }

    // Create the JobRoller pages and assign the templates to them
    function createPages() {

        if($em_vacation_request_page_id = get_option('em_vacation_request_page_id')){
            if ( FALSE === get_post_status( $em_vacation_request_page_id ) ) {
                $post_exist = false;
            }else{
                $post_exist = true;
                update_option('em_vacation_request_page_id', $em_vacation_request_page_id);
            }
        }else{
            $post_exist = false;
        }

       if(!$post_exist) {

            // then create the edit item page
            $my_page = array(
                'post_status' => 'publish',
                'post_type' => 'page',
                'post_author' => 1,
                'post_name' => 'vacation-requests',
                'post_title' =>  __('Vacation Requests', 'employees-vacation-manager')
            );

            // Insert the page into the database
            $page_id = wp_insert_post($my_page);

            update_post_meta($page_id, '_wp_page_template', 'tmp-vacation-request.php');

            update_option('em_vacation_request_page_id', $page_id);
        }
    }

    public function addActionsAndFilters() {
        add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));
        add_action( 'em_info_table', array(&$this, 'getInfoTable'), 10, 1);
        add_action( 'em_user_vacation_requests_table', array(&$this, 'getUserVacationRequests'), 10, 1);
        add_action( 'wp_loaded', array(&$this, 'saveVacationRequest'));
        add_action( 'em_vacation_request_filter_form', array(&$this, 'getVacationRequestFilterForm'));
        add_action( 'em_show_vacation_request_details', array(&$this, 'showVacationRequestDetails'), 10, 1);
        add_action( 'em_frontend_table_pager', array(&$this, 'getFrontendTablePager'), 10, 2);
        add_action( 'admin_enqueue_scripts', function($hook){
                if ( 'employees-vacation-manager-vacation-requests' != $hook ) {
                    EmployeesManager_Utils::enqueue_scripts_and_styles();
                }
            }
        );

        //short code
        add_shortcode('ShowVacationCalender', array($this,'calenderOverviewPage'));
        add_action('wp_enqueue_scripts', function(){EmployeesManager_Utils::enqueue_calender_scripts_and_styles();});
    }

    public function getEditableRoles() {
        $editable_roles = apply_filters('editable_roles', $this->getAllRoles());
        return $editable_roles;
    }

    public function getAllRoles() {
        global $wp_roles;
        $all_roles = $wp_roles->roles;
        return $all_roles;
    }

    public function saveVacationRequest() {

        if (!$this->canUserRequestVacation()) {
            add_filter( 'employees-vacation-manager-notification', function(){
                return array("type" => "err", "text" => __('You do not have sufficient permissions to access this page.', "employees-vacation-manager"));
            });
        }else{
            global $wpdb;
            $userdata = wp_get_current_user(); // grabs the user info and puts into vars
            $table_name = $wpdb->prefix .  'em_vacation_request';
            $date_format = (get_option('em_date_format'))? get_option('em_date_format'): 'DD.MM.YYYY';
            $php_date_format = array('DD.MM.YYYY' =>'d.m.Y' , 'DD-MM-YYYY'  => 'd-m-Y', 'MM.DD.YYYY'  => 'm.d.Y',
                                     'MM-DD-YYYY'  => 'm-d-Y', 'YYYY.MM.DD'  => 'Y.m.d', 'YYYY-MM-DD'  => 'Y-m-d'
            );

            $raw_form_key = $userdata->user_login.$userdata->user_email.$userdata->ID;

            if(isset($_POST) && isset($_POST['form-key']) && $_POST['form-key'] != ""){
                $form_key = $_POST['form-key'];
                $decrypt_form_key = EmployeesManager_Utils::encrypt_decrypt("decrypt", $form_key, $userdata->user_pass);
                if($decrypt_form_key == $raw_form_key){
                    if(isset($_POST['date-range']) && $_POST['date-range'] != ""  && isset($_POST['vacation-type']) && $_POST['vacation-type'] != "" && is_user_logged_in()){
                        $dates = explode(' - ', $_POST['date-range']);
                        $comment = (isset($_POST['vacation-comment']) && $_POST['vacation-comment'] != "" )? $_POST['vacation-comment']  : "";

                        if(count($dates) == 2){
                            $fromDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[0]));
                            $toDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[1]));

                            $validation = $this->validateDates($userdata->ID, $fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));

                            if($validation){
                                if($this->checkUserAvailableVacationDays($dates, $userdata->ID, $php_date_format[$date_format], $_POST['vacation-type'])){
                                    $now = current_time('mysql', false);
                                    $history = array();
                                    $history[] = array($now => array('created' => $userdata->display_name));

                                    $wpdb->insert($table_name, array("user" => $userdata->ID, "time" => $now, "from_date" => date('Y-m-d', strtotime($fromDate->format('Y-m-d'))),
                                                                     "to_date" => date('Y-m-d', strtotime($toDate->format('Y-m-d'))), "comment" => $comment,
                                                                     "status" => 0, "type" => $_POST['vacation-type'], "history" =>  serialize($history)));

                                    $request_id = $wpdb->insert_id;

                                    if($_POST['vacation-type'] == 'paid-vacation'){

                                        $this->saveVacationRequestDates($request_id, $fromDate, $toDate, $userdata->ID);
                                    }

                                    $headers[] ='Content-Type: text/html; charset=UTF-8';
                                    $headers[] = 'From: '.get_option('blogname').' <'.get_option('admin_email').'>';
                                    $to = get_option('admin_email');
                                    $subject = __('You have received a new vacation request', 'employees-vacation-manager');
                                    $message = __('You have received a new vacation request form ', 'employees-vacation-manager').$userdata->display_name. "<br/>";
                                    $link =  add_query_arg('showvacationrequest', $request_id, admin_url('admin.php?page=employees-manager-vacation-requests'));
                                    $message .= __('Details: ', 'employees-vacation-manager'). '<a href="'.$link.'" >'.$link . '</a>' ;

                                    wp_mail( $to, $subject, $message, $headers);

                                    add_filter( 'employees-vacation-manager-notification', function(){
                                        return array("type" => "success", "text" => __('The changes have been saved successfully', "employees-vacation-manager"));
                                    });
                                }else{
                                    add_filter( 'employees-vacation-manager-notification', function(){
                                        return array("type" => "err", "text" => __('Maximum available vacation days exceeded.', "employees-vacation-manager"));
                                    });
                                }
                            }else{
                                add_filter( 'employees-vacation-manager-notification', function(){
                                    return array("type" => "err", "text" => __('There are conflicts with other requests.', "employees-vacation-manager"));
                                });
                            }
                        }
                    }
                }else{
                    add_filter( 'employees-vacation-manager-notification', function(){
                        return array("type" => "err", "text" => __('permission denied', "employees-vacation-manager"));
                    });
                }
            }

        }
    }

    public function getInfoTable($userId){
        global $wpdb;
        $request_dates_table_name = $this->prefixTableName('vacation_request_dates');
        $current_year = date('Y');
        $vacations_days = ($vacations_days = $this->getOption('available_paid_vacation'))? $vacations_days : 30;
        $total_first_year = $wpdb->get_var( "SELECT SUM(working_days) FROM ".$request_dates_table_name." WHERE user=".$userId." AND year =".($current_year - 1));
        $total_current_year = $wpdb->get_var( "SELECT SUM(working_days) FROM ".$request_dates_table_name." WHERE user=".$userId." AND year =".$current_year );
        $total_second_year = $wpdb->get_var( "SELECT SUM(working_days) FROM ".$request_dates_table_name." WHERE user=".$userId." AND year =".((int)$current_year + 1 ));
    ?>
        <table class="plugin-options-table">
            <tr>
                <th><?php _e('amount of available paid vacation a year', 'employees-vacation-manager') ?></th>
                <td><?php echo $vacations_days?></td>
            </tr>
            <tr>
                <th><?php echo __('Planned vacations for the year', 'employees-vacation-manager').' '. ($current_year - 1) ?></th>
                <td><?php echo $total_first_year ?></td>
            </tr>
            <tr>
                <th><?php echo __('Planned vacations for the year', 'employees-vacation-manager').' '. $current_year  ?></th>
                <td><?php echo $total_current_year ?></td>
            </tr>
            <tr>
                <th><?php echo __('Planned vacations for the year', 'employees-vacation-manager').' '. ($current_year + 1) ?></th>
                <td><?php echo $total_second_year ?></td>
            </tr>
        </table>

    <?php
    }

    public function getUserVacationRequests($vacation_requests){
        $statuses = array(0 => __('In Progress', 'employees-vacation-manager'), 1 => __('Approved', 'employees-vacation-manager'), 2 => __('Rejected', 'employees-vacation-manager') );
        $date_format = (get_option('em_date_format'))? get_option('em_date_format'): 'DD.MM.YYYY';
        $php_date_format = $this->getPhpDateFomat();
        $current_page = get_query_var( 'em-page', 1 );
        $per_page = 10;
        $total_rows = count($vacation_requests);
        $total_pages = ceil(($total_rows > 0 && $total_rows > $per_page)? $total_rows / $per_page : 1);
        $first_item = ($current_page == 1) ? 0 : $per_page * ($current_page - 1);
        $vacation_requests = ($total_rows > $per_page)? array_slice($vacation_requests, $first_item, $per_page): $vacation_requests;
        ?>
        <table data-tablesaw-sortable>
            <thead>
            <tr>
                <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col><?php  _e('Created on', 'employees-vacation-manager')  ?></th>
                <th data-tablesaw-sortable-col><?php  _e('Vacation type', 'employees-vacation-manager')  ?></th>
                <th data-tablesaw-sortable-col data-sortable-numeric><?php  _e('From Date', 'employees-vacation-manager')  ?></th>
                <th data-tablesaw-sortable-col data-sortable-numeric><?php  _e('To Date', 'employees-vacation-manager')  ?></th>
                <th data-tablesaw-sortable-col data-sortable-numeric><?php  _e('Days', 'employees-vacation-manager')  ?></th>
                <th data-tablesaw-sortable-col><?php  _e('Status', 'employees-vacation-manager')  ?></th>
                <th data-tablesaw-sortable-col><?php  _e('Comment', 'employees-vacation-manager')  ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if (!empty($vacation_requests)) {
                foreach ($vacation_requests as $vacation_request) {
                    $vacation_request = $this->parseArrayToObject($vacation_request);
                    $time = date_create($vacation_request->time);
                    $from_date = date_create($vacation_request->from_date);
                    $to_date = date_create($vacation_request->to_date);
                    $interval = EmployeesManager_Utils::getWorkingDays($vacation_request->from_date, $vacation_request->to_date);
                    $vacation_type = array('paid-vacation' => "Paid vacation", 'unpaid-vacation' => "Unpaid vacation");
                    $comment_class = (strlen($vacation_request->comment) > 20)? 'data-hover' : '';
                    ?>
                    <tr>
                        <td><?php echo date_format($time,  $php_date_format[$date_format].' H:i:s') ?></td>
                        <td><?php _e($vacation_type[$vacation_request->type], 'employees-vacation-manager')  ?></td>
                        <td><?php echo date_format($from_date,  $php_date_format[$date_format])  ?></td>
                        <td><?php echo date_format($to_date,  $php_date_format[$date_format]) ?></td>
                        <td><?php echo  $interval ?></td>
                        <td><?php echo $statuses[$vacation_request->status] ?></td>
                        <td><div class="<?php echo $comment_class ?>"> <?php echo $vacation_request->comment  ?></div></td>
                    </tr>
                <?php
                }
            }
            ?>
            </tbody>
        </table>
    <?php
        do_action('em_frontend_table_pager', $current_page, $total_pages);
    }


    public function getVacationRequestFilterForm(){
        $users = get_users();
        $admin_filter_user = (isset($_GET['em-f-user']))? $_GET['em-f-user'] : "";
        $admin_filter_date_range_start = (isset($_GET['em-dr-start']))? $_GET['em-dr-start'] :  __('Vacation start (Date range)', 'employees-vacation-manager') ;
        $admin_filter_date_range_end = (isset($_GET['em-dr-end']))? $_GET['em-dr-end'] :   __('Vacation end (Date range)', 'employees-vacation-manager');
        $admin_filter_vacation_type = (isset($_GET['em-v-type']))? $_GET['em-v-type'] : "";
        ?>
        <div style="margin:0 4px">
            <form  method="get" autocomplete="off">
                <input type="hidden" name="page" value="employees-manager-vacation-requests" />
               <select name="em-f-user" id="admin-filter-user">
                    <option value="-1"><?php _e('User', 'employees-vacation-manager') ?></option>
                    <?php foreach($users as $user_item): ?>
                        <option <?php if($admin_filter_user == $user_item->ID) echo 'selected="selected"'?> value="<?php echo $user_item->ID ?>"><?php echo  $user_item->last_name . ', ' . $user_item->first_name?></option>
                    <?php endforeach ?>
                </select>
                <select name="em-v-type" id="admin-filter-vacation-type">
                    <option value="-1"><?php _e('Vacation type', 'employees-vacation-manager') ?></option>
                    <option value="paid-vacation" <?php if($admin_filter_vacation_type == "paid-vacation") echo 'selected="selected"'?> ><?php  _e('Paid vacation', 'employees-vacation-manager')  ?></option>
                    <option value="unpaid-vacation" <?php if($admin_filter_vacation_type == "unpaid-vacation") echo 'selected="selected"'?> ><?php  _e('Unpaid vacation', 'employees-vacation-manager')  ?></option>
                </select>
                <input type="text" name="em-dr-start" id="admin-filter-date-range-start" value="<?php echo $admin_filter_date_range_start?>"  size="25">
                <input type="text" name="em-dr-end" id="admin-filter-date-range-end" value="<?php echo $admin_filter_date_range_end?>"  size="25">
                <input type="submit" class="button" value="<?php _e('Apply Filter', 'employees-vacation-manager') ?>" id="em_account_submit">
            </form>
        </div>
    <?php
    }

    public function showVacationRequestDetails($vacation_request){
        global $wpdb;
        $statuses = array(0 => __('In Progress', 'employees-vacation-manager'), 1 => __('Approved', 'employees-vacation-manager'), 2 => __('Rejected', 'employees-vacation-manager') );
        $table_name = $this->prefixTableName('vacation_request');
        $row = $wpdb->get_row( "SELECT * FROM ".$table_name. " WHERE id=".$vacation_request );
        $user_info = get_userdata($row->user);
        $time = date_create($row->time);
        $from_date = date_create($row->from_date);
        $to_date = date_create($row->to_date);
        $interval = $from_date->diff($to_date);
        $history = unserialize($row->history);
        $php_date_format = $this->getPhpDateFomat();
        $date_format = ($date_format = $this->getOption('date_format'))? $date_format: 'DD.MM.YYYY';
        $user_name = ($user_info->last_name != "")?$user_info->last_name . ', ' . $user_info->first_name : $user_info->display_name;
        ?>
        <h1><?php _e('Vacation Request Details', 'employees-vacation-manager') ?></h1>
        <div id="vacation-request-detail" class="vacation-request-detail">
            <div id="vacation-request-detail-content" class="vacation-request-detail-content">
                <table>
                    <tr>
                        <th><?php _e('Created', 'employees-vacation-manager') ?></th>
                        <td><?php echo  date_format($time, $php_date_format[$date_format])?></td>
                    </tr>
                    <tr>
                        <th><?php _e('User', 'employees-vacation-manager') ?></th>
                        <td><?php echo $user_name ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('Vacation type', 'employees-vacation-manager') ?></th>
                        <td><?php _e(($row->type == "paid-vacation" ? "Paid vacation" : "Unpaid vacation"), "employees-vacation-manager") ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('From Date', 'employees-vacation-manager') ?></th>
                        <td><?php echo date_format($from_date, $php_date_format[$date_format])  ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('To Date', 'employees-vacation-manager') ?></th>
                        <td><?php echo date_format($to_date, 'd.m.Y')  ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('Days', 'employees-vacation-manager') ?></th>
                        <td><?php echo $interval->format('%R%a') ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('Status', 'employees-vacation-manager') ?></th>
                        <td><?php echo $statuses[$row->status] ?></td>
                    </tr>
                    <tr>
                        <th><?php _e('Comment', 'employees-vacation-manager') ?></th>
                        <td><?php echo $row->comment ?></td>
                    </tr>
                    <tr>
                        <th colspan="2"><h3><?php _e('History', 'employees-vacation-manager') ?></h3></th>
                    </tr>
                    <?php
                    foreach($history as  $history_item):?>
                        <?php foreach($history_item as $history_item_key => $history_item_item):
                            $history_item_date = date_create($history_item_key);
                            if(is_array($history_item_item)):
                                ?>
                                <tr>
                                    <th><?php echo date_format($history_item_date, $php_date_format[$date_format].' H:i:s') ?></th>
                                    <td><?php echo __(key($history_item_item), 'employees-vacation-manager'). __(' by ', 'employees-vacation-manager'). current($history_item_item) ?></td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endforeach ?>
                </table>
            </div>
            <a class="button" href="<?php echo wp_get_referer() ?>"><?php _e('Back to the overview', 'employees-vacation-manager') ?></a>
        </div>


    <?php
    }

    public function  getFrontendTablePager($current_page, $total_pages){
        $page_uri = get_bloginfo('url').'/'.get_page_uri( get_option('em_vacation_request_page_id') ) ;
        ?>

        <div id="pager" class="pager">
            <div class="em-clear">
                <?php if ($current_page > 1):?>
                    <div class="em-page-links">
                        <a href="<?php echo $page_uri."/em-page/1/" ?>"><img src="<?php echo  plugins_url( 'employees-vacation-manager/assets/images/first.png' )?>" class="first"/></a>
                        <a href="<?php echo $page_uri."/em-page/".($current_page - 1) ?>"><img src="<?php echo  plugins_url( 'employees-vacation-manager/assets/images/prev.png' )?>" class="prev"/></a>
                    </div>
                <?php endif ;?>
                <div class="em-page-count"><?php  _e('Page ', 'employees-vacation-manager')  ?><?php echo $current_page."/".$total_pages ?></div>
                <div class="em-page-links em-go-to-page">
                    <label for="em-go-to-page"><?php  _e('Go to', 'employees-vacation-manager')  ?></label>
                    <select id="em-go-to-page">
                        <option></option>
                        <?php for($i = 1; $i <= $total_pages; $i++): ?>
                            <option value="<?php echo $page_uri."/em-page/".$i ?>"><?php echo $i ?></option>
                        <?php endfor ?>
                    </select>
                    <script>
                        document.getElementById('em-go-to-page').onchange = function() {
                            if (this.selectedIndex!==0) {
                                window.location.href = this.value;
                            }
                        }
                    </script>
                </div>
                <?php if ($total_pages > 1 && $current_page < $total_pages):?>
                    <div class="em-page-links">
                        <a href="<?php echo $page_uri."/em-page/".($current_page + 1) ?>"><img src="<?php echo  plugins_url( 'employees-vacation-manager/assets/images/next.png' )?>" class="next"/></a>
                        <a href="<?php echo $page_uri."/em-page/".$total_pages?>"><img src="<?php echo  plugins_url( 'employees-vacation-manager/assets/images/last.png' )?>" class="last"/></a>
                    </div>
                <?php endif ;?>

            </div>
        </div>
        <br class="em-clear"/>


    <?php
    }

}
