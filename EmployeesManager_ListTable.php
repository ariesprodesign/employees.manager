<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}
include_once('EmployeesManager_Utils.php');

class EmployeesManager_ListTable extends WP_List_Table {

    public $items;

	/** Class constructor */
	public function __construct($items) {
        $this->items = $this->fill_items($items);
		parent::__construct( [
			'singular' => __( 'Vacation requests Overview', 'employees-vacation-manager' ), //singular name of the listed records
			'plural'   => __( 'Vacation requests Overview', 'employees-vacation-manager' ), //plural name of the listed records
			'ajax'     => false, //does this table support ajax?
            'screen'   => 'employees-vacation-manager'
		] );

	}



	/** Text displayed when no customer data is available */
	public function no_items() {
		_e( 'No Vacation Requests available.', 'employees-vacation-manager' );
	}


	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="vacation_request[]" value="%s" />', $item['id']
		);
	}



	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = [
			'cb'      => '<input type="checkbox" />',
			'created-on'    => __( 'Created on', 'employees-vacation-manager' ),
			'user' => __( 'User', 'employees-vacation-manager' ),
			'vacation-type'    => __( 'Vacation type', 'employees-vacation-manager' ),
            'from-date'    => __( 'From Date', 'employees-vacation-manager' ),
            'to-date'    => __( 'To Date', 'employees-vacation-manager' ),
            'days'    => __( 'Days', 'employees-vacation-manager' ),
            'status'    => __( 'Status', 'employees-vacation-manager' ),
            'details'    => __( 'Details', 'employees-vacation-manager' )
		];

		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
            'created-on' => array( 'created-on', true ),
			'user' => array( 'user', true ),
			'from-date' => array( 'from-date', true ),
            'to-date' => array( 'to-date', true ),
            'days' => array( 'days', true ),
            'status' => array( 'status', true ),
            'vacation-type' => array( 'vacation-type', true ),
		);

		return $sortable_columns;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			'bulk-approve' =>  __('Authorize', 'employees-vacation-manager'),
            'bulk-decline' =>  __('Decline', 'employees-vacation-manager'),
            'bulk-delete' => __('Delete', 'employees-vacation-manager')
		];

		return $actions;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( 'vacation_request_per_page', 10);
		$current_page = $this->get_pagenum();
		$total_items  = count($this->items);

        usort( $this->items, array( &$this, 'sort_data' ) );
        $data = $this->get_vacation_requests( $per_page, $current_page );

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );

		$this->items = $data;
	}



    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'cb':
            case 'created-on':
            case 'user':
            case 'vacation-type':
            case 'from-date':
            case 'to-date':
            case 'days':
            case 'status':
            case 'details':
                return $item[$column_name];
            default:
                return "" ;
        }
    }



    protected  function fill_items($items){
        $data = array();
        $php_date_format = $this->getPhpDateFomat();
        $date_format = ($date_format = get_option('em_date_format'))? $date_format: 'DD.MM.YYYY';
        $statuses = array(0 => __('In Progress', 'employees-vacation-manager'), 1 => __('Approved', 'employees-vacation-manager'), 2 => __('Rejected', 'employees-vacation-manager') );

        if (!empty($items)) {

            foreach ($items as $vacation_request) {
                $user_info = get_userdata($vacation_request->user);
                $time = date_create($vacation_request->time);
                $from_date = date_create($vacation_request->from_date);
                $to_date = date_create($vacation_request->to_date);
                $interval = EmployeesManager_Utils::getWorkingDays($vacation_request->from_date, $vacation_request->to_date);
                $user_name = ($user_info->last_name != "")?$user_info->last_name . ', ' . $user_info->first_name : $user_info->display_name;

                $data[] = array(
                    'id'      => $vacation_request->id,
                    'created-on'    => date_format($time, $php_date_format[$date_format].' H:i:s') ,
                    'user' => $user_name ,
                    'vacation-type'    => __(($vacation_request->type == "paid-vacation" ? "Paid vacation" : "Unpaid vacation"), "employees-vacation-manager"),
                    'from-date'    => date_format($from_date, $php_date_format[$date_format]) ,
                    'to-date'    => date_format($to_date, $php_date_format[$date_format]),
                    'days'    => $interval,
                    'status'    => $statuses[$vacation_request->status],
                    'details'    => '<a href="'. add_query_arg('showvacationrequest', $vacation_request->id, admin_url('admin.php?page=employees-manager-vacation-requests')).'">'. __('Details', 'employees-vacation-manager').'</a>'
                );

            }
        }
        return $data;
    }

    protected  function get_vacation_requests($per_page, $current_page){

        if (!empty($this->items)) {

            $count = count($this->items);
            $first_item = ($current_page == 1) ? 0 : $per_page * ($current_page - 1);
            $pageItems = ($count > $per_page)? array_slice($this->items, $first_item, $per_page): $this->items;

            return $pageItems;
        }
        return $this->items;;
    }


    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'created-on';
        $order = 'desc';
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
        if(is_numeric($a[$orderby]) && is_numeric($b[$orderby])){
            $result = ( $a[$orderby] > $b[$orderby] ) ? 1 : -1;
        }else{
            $result = strcmp( $a[$orderby], $b[$orderby] );
        }

        if($order === 'asc')
        {
            return $result;
        }
        return -$result;
    }


    protected function getPhpDateFomat(){
        $php_date_format = array('DD.MM.YYYY' =>'d.m.Y' , 'DD-MM-YYYY'  => 'd-m-Y', 'MM.DD.YYYY'  => 'm.d.Y',
                                 'MM-DD-YYYY'  => 'm-d-Y', 'YYYY.MM.DD'  => 'Y.m.d', 'YYYY-MM-DD'  => 'Y-m-d'
        );
        return $php_date_format;
    }




    public function process_bulk_action() {
        global $wpdb;
        $table_name = $wpdb->prefix."em_vacation_request";
        $dates_table_name = $wpdb->prefix."em_vacation_request_dates";
        $current_user = wp_get_current_user();

        // If the delete bulk action is triggered
        if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' ) || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )) {

            $delete_ids = esc_sql( $_POST['vacation_request'] );

            // loop over the array of record IDs and delete them
            foreach ( $delete_ids as $id ) {
                $wpdb->delete( $table_name, array( 'id' => $id ) );
                $wpdb->delete( $dates_table_name, array( 'request_id' => $id ) );
            }

            wp_redirect( esc_url( add_query_arg() ) );
            exit;
        }elseif ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-approve' ) || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-approve' )) {

            $approve_ids = esc_sql( $_POST['vacation_request'] );

            foreach($approve_ids as $vacation_request){
                $row = $wpdb->get_row( "SELECT * FROM ".$table_name. " WHERE id=".$vacation_request );
                if($row->status != 1){
                    $history = unserialize($row->history);
                    $history = (is_array($history))? $history : array();
                    $history[] = array(current_time('mysql', false) => array('approved' => $current_user->display_name));
                    $wpdb->update( $table_name, array('status' => 1, "history" => serialize($history) ), array('id' => $vacation_request));
                    $this->sendUserNotificationEmail($row, 'approved');
                }
            }

            wp_redirect( esc_url( add_query_arg() ) );
            exit;
        }elseif ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-decline' ) || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-decline' )) {

            $decline_ids = esc_sql( $_POST['vacation_request'] );

            foreach($decline_ids as $vacation_request){
                $row = $wpdb->get_row( "SELECT * FROM ".$table_name. " WHERE id=".$vacation_request );
                if($row->status != 2){
                    $history = unserialize($row->history);
                    $history = (is_array($history))? $history : array();
                    $history[] = array(current_time('mysql', false) => array('rejected' => $current_user->display_name));
                    $wpdb->update( $table_name, array('status' => 2, "history" =>  serialize($history) ), array('id' => $vacation_request));
                    $this->sendUserNotificationEmail($row, 'declined');
                }
            }

            wp_redirect( esc_url( add_query_arg() ) );
            exit;
        }
    }

    protected function sendUserNotificationEmail( $row, $status){
        $user_info = get_userdata($row->user);
        $time = date_create($row->time);
        $from_date = date_create($row->from_date);
        $to_date = date_create($row->to_date);
        $date_format = ($date_format = get_option('em_date_format'))? $date_format: 'DD.MM.YYYY';
        $php_date_format = $this->getPhpDateFomat();

        $headers = array();
        $headers[] ='Content-Type: text/html; charset=UTF-8';
        $headers[] = 'From: '.get_option('blogname').' <'.get_option('admin_email').'>';
        $to = $user_info->user_email;

        $subject = __('Your request for vacation was '.$status, 'employees-vacation-manager');
        $message =  __('Your request for vacation was '.$status, 'employees-vacation-manager'). "<br/>";
        $message .= __('Details: ', 'employees-vacation-manager'). "<br/>";
        $message .= __('Created on', 'employees-vacation-manager').': '.date_format($time, $php_date_format[$date_format]). "<br/>" ;
        $message .= __('From Date', 'employees-vacation-manager').': '.date_format($from_date, $php_date_format[$date_format]). "<br/>" ;
        $message .= __('To Date', 'employees-vacation-manager').': '.date_format($to_date, $php_date_format[$date_format]) ;

        wp_mail( $to, $subject, $message, $headers);
    }
}


