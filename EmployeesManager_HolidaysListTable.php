<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class EmployeesManager_HolidaysListTable extends WP_List_Table {

    public $items;

	/** Class constructor */
	public function __construct() {
        global $wpdb;
        $holidays_table_name = $wpdb->prefix."em_holidays_dates";
        $items = $wpdb->get_results( "SELECT * FROM ".$holidays_table_name );
        $this->items = $this->fill_items($items);
		parent::__construct( [
			'singular' => __( 'Holidays Overview', 'employees-vacation-manager' ), //singular name of the listed records
			'plural'   => __( 'Holidays Overview', 'employees-vacation-manager' ), //plural name of the listed records
			'ajax'     => false, //does this table support ajax?
            'screen'   => 'holidays-vacation-manager'
		] );

	}



	/** Text displayed when no customer data is available */
	public function no_items() {
		_e( 'No Holidays Requests available.', 'employees-vacation-manager' );
	}


	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="holidays[]" value="%s" />', $item['id']
		);
	}



	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = [
			'cb'      => '<input type="checkbox" />',
			'date'    => __( 'Date', 'employees-vacation-manager' ),
			'comment' => __( 'Comment', 'employees-vacation-manager' )
		];

		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
            'date' => array( 'date', true ),
			'comment' => array( 'comment', true ),
		);

		return $sortable_columns;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
            'bulk-delete' => __('Delete', 'employees-vacation-manager')
		];

		return $actions;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( 'holidays_per_page', 10);
		$current_page = $this->get_pagenum();
		$total_items  = count($this->items);

        usort( $this->items, array( &$this, 'sort_data' ) );
        $data = $this->get_holidays( $per_page, $current_page );

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );

		$this->items = $data;
	}



    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'cb':
            case 'date':
            case 'comment':
                return $item[$column_name];
            default:
                return "" ;
        }
    }



    protected  function fill_items($items){
        $data = array();
        $php_date_format = $this->getPhpDateFomat();
        $date_format = ($date_format = get_option('em_date_format'))? $date_format: 'DD.MM.YYYY';

        if (!empty($items)) {

            foreach ($items as $holiday) {
                $date = date_create($holiday->date);

                $data[] = array(
                    'id'      => $holiday->id,
                    'date'    => date_format($date, $php_date_format[$date_format]) ,
                    'comment'    => $holiday->comment,
                );

            }
        }
        return $data;
    }

    protected  function get_holidays($per_page, $current_page){

        if (!empty($this->items)) {

            $count = count($this->items);
            $first_item = ($current_page == 1) ? 0 : $per_page * ($current_page - 1);
            $pageItems = ($count > $per_page)? array_slice($this->items, $first_item, $per_page): $this->items;

            return $pageItems;
        }
        return $this->items;
    }


    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'date';
        $order = 'desc';
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
        if(is_numeric($a[$orderby]) && is_numeric($b[$orderby])){
            $result = ( $a[$orderby] > $b[$orderby] ) ? 1 : -1;
        }else{
            $result = strcmp( $a[$orderby], $b[$orderby] );
        }

        if($order === 'asc')
        {
            return $result;
        }
        return -$result;
    }


    protected function getPhpDateFomat(){
        $php_date_format = array('DD.MM.YYYY' =>'d.m.Y' , 'DD-MM-YYYY'  => 'd-m-Y', 'MM.DD.YYYY'  => 'm.d.Y',
                                 'MM-DD-YYYY'  => 'm-d-Y', 'YYYY.MM.DD'  => 'Y.m.d', 'YYYY-MM-DD'  => 'Y-m-d'
        );
        return $php_date_format;
    }




    public function process_bulk_action() {
        global $wpdb;
        $table_name = $wpdb->prefix."em_holidays_dates";

        // If the delete bulk action is triggered
        if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete') || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' ) ) {

            $delete_ids = esc_sql( $_POST['holidays'] );

            // loop over the array of record IDs and delete them
            foreach ( $delete_ids as $id ) {
                $wpdb->delete( $table_name, array( 'id' => $id ) );
            }

            wp_redirect( esc_url( add_query_arg() ) );
            exit;
        }elseif(isset($_POST['em_holidays_submit'])){
            if(isset($_POST['holiday-date'])){

                $php_date_format = $this->getPhpDateFomat();
                $date_format = ($date_format = get_option('em_date_format'))? $date_format: 'DD.MM.YYYY';
                $comment = (isset($_POST['holiday-comment']))? $_POST['holiday-comment'] : " ";
                $date = DateTime::createFromFormat($php_date_format[$date_format], trim($_POST['holiday-date']));
                $find_date = $wpdb->get_var( "SELECT sum(date) FROM ".$table_name." WHERE date='".$date->format('Y-m-d')."'");

                if($find_date == NULL){
                    $wpdb->insert($table_name, array("date" => date('Y-m-d', strtotime($date->format('Y-m-d'))), "comment" => $comment));
                }

            }
            wp_redirect( esc_url( add_query_arg() ) );
            exit;
        }
    }
}


