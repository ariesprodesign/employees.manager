<?php
/**
 * Employees Vacation Manager Extension
 *
 * @category     Extension
 * @copyright    Copyright © 2016 Aries Prodesign (http://ariesprodesign.de/)
 * @author       Aries Prodesign. Eduardo Garces Hernandez
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version     Release: 1.0.0
 */
include_once('EmployeesManager_Utils.php');

class EmployeesManager_OptionsManager {

    public function getOptionNamePrefix() {
        return 'em_';
    }


    /**
     * Define your options meta data here as an array, where each element in the array
     * @return array of key=>display-name and/or key=>array(display-name, choice1, choice2, ...)
     * key: an option name for the key (this name will be given a prefix when stored in
     * the database to ensure it does not conflict with other plugin options)
     * value: can be one of two things:
     *   (1) string display name for displaying the name of the option to the user on a web page
     *   (2) array where the first element is a display name (as above) and the rest of
     *       the elements are choices of values that the user can select
     * e.g.
     * array(
     *   'item' => 'Item:',             // key => display-name
     *   'rating' => array(             // key => array ( display-name, choice1, choice2, ...)
     *       'CanDoOperationX' => array('Can do Operation X', 'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber'),
     *       'Rating:', 'Excellent', 'Good', 'Fair', 'Poor')
     */
    public function getOptionMetaData() {
        return array();
    }

    /**
     * @return array of string name of options
     */
    public function getOptionNames() {
        return array_keys($this->getOptionMetaData());
    }

    /**
     * Override this method to initialize options to default values and save to the database with add_option
     * @return void
     */
    protected function initOptions() {
    }

    /**
     * Cleanup: remove all options from the DB
     * @return void
     */
    protected function deleteSavedOptions() {
        $optionMetaData = $this->getOptionMetaData();
        if (is_array($optionMetaData)) {
            foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                $prefixedOptionName = $this->prefix($aOptionKey); // how it is stored in DB
                delete_option($prefixedOptionName);
            }
        }
    }

    /**
     * @return string display name of the plugin to show as a name/title in HTML.
     * Just returns the class name. Override this method to return something more readable
     */
    public function getPluginDisplayName() {
        return __('Employees Vacation', 'employees-vacation-manager');
    }

    protected function getPhpDateFomat(){
        $php_date_format = array('DD.MM.YYYY' =>'d.m.Y' , 'DD-MM-YYYY'  => 'd-m-Y', 'MM.DD.YYYY'  => 'm.d.Y',
                                 'MM-DD-YYYY'  => 'm-d-Y', 'YYYY.MM.DD'  => 'Y.m.d', 'YYYY-MM-DD'  => 'Y-m-d'
        );
        return $php_date_format;
    }

    protected function getJavaScriptDateFomat(){
        $php_date_format = array('DD.MM.YYYY' =>'dd.mm.yy' , 'DD-MM-YYYY'  => 'dd-mm-yy', 'MM.DD.YYYY'  => 'mm.dd.yy',
                                 'MM-DD-YYYY'  => 'mm-dd-yy', 'YYYY.MM.DD'  => 'yy.mm.dd', 'YYYY-MM-DD'  => 'yy-mm-dd'
        );
        return $php_date_format;
    }

    /**
     * Get the prefixed version input $name suitable for storing in WP options
     * Idempotent: if $optionName is already prefixed, it is not prefixed again, it is returned without change
     * @param  $name string option name to prefix. Defined in settings.php and set as keys of $this->optionMetaData
     * @return string
     */
    public function prefix($name) {
        $optionNamePrefix = $this->getOptionNamePrefix();
        if (strpos($name, $optionNamePrefix) === 0) { // 0 but not false
            return $name; // already prefixed
        }
        return $optionNamePrefix . $name;
    }

    /**
     * Remove the prefix from the input $name.
     * Idempotent: If no prefix found, just returns what was input.
     * @param  $name string
     * @return string $optionName without the prefix.
     */
    public function &unPrefix($name) {
        $optionNamePrefix = $this->getOptionNamePrefix();
        if (strpos($name, $optionNamePrefix) === 0) {
            return substr($name, strlen($optionNamePrefix));
        }
        return $name;
    }

    /**
     * A wrapper function delegating to WP get_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param $default string default value to return if the option is not set
     * @return string the value from delegated call to get_option(), or optional default value
     * if option is not set.
     */
    public function getOption($optionName, $default = null) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        $retVal = get_option($prefixedOptionName);
        if (!$retVal && $default) {
            $retVal = $default;
        }
        return $retVal;
    }

    /**
     * A wrapper function delegating to WP delete_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param  $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @return bool from delegated call to delete_option()
     */
    public function deleteOption($optionName) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        return delete_option($prefixedOptionName);
    }

    /**
     * A wrapper function delegating to WP add_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param  $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param  $value mixed the new value
     * @return null from delegated call to delete_option()
     */
    public function addOption($optionName, $value) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        return add_option($prefixedOptionName, $value);
    }

    /**
     * A wrapper function delegating to WP add_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * @param  $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param  $value mixed the new value
     * @return null from delegated call to delete_option()
     */
    public function updateOption($optionName, $value) {
        $prefixedOptionName = $this->prefix($optionName); // how it is stored in DB
        return update_option($prefixedOptionName, $value);
    }

    /**
     * A Role Option is an option defined in getOptionMetaData() as a choice of WP standard roles, e.g.
     * 'CanDoOperationX' => array('Can do Operation X', 'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber')
     * The idea is use an option to indicate what role level a user must minimally have in order to do some operation.
     * So if a Role Option 'CanDoOperationX' is set to 'Editor' then users which role 'Editor' or above should be
     * able to do Operation X.
     * Also see: canUserDoRoleOption()
     * @param  $optionName
     * @return string role name
     */
    public function getRoleOption($optionName) {
        $roleAllowed = $this->getOption($optionName);
        if (!$roleAllowed || $roleAllowed == '') {
            $roleAllowed = 'Administrator';
        }
        return $roleAllowed;
    }

    /**
     * Given a WP role name, return a WP capability which only that role and roles above it have
     * http://codex.wordpress.org/Roles_and_Capabilities
     * @param  $roleName
     * @return string a WP capability or '' if unknown input role
     */
    protected function roleToCapability($roleName) {
        switch ($roleName) {
            case 'administrator':
                return 'manage_options';
            case 'editor':
                return 'publish_pages';
            case 'author':
                return 'publish_posts';
            case 'contributor':
                return 'edit_posts';
            case 'subscriber':
                return 'read';
            case 'anyone':
                return 'read';
        }
        return '';
    }

    /**
     * @param $roleName string a standard WP role name like 'Administrator'
     * @return bool
     */
    public function isUserRoleEqualOrBetterThan($roleName) {
        if ('anyone' == $roleName) {
            return true;
        }
        $capability = $this->roleToCapability($roleName);
        return current_user_can($capability);
    }

    /**
     * @param  $optionName string name of a Role option (see comments in getRoleOption())
     * @return bool indicates if the user has adequate permissions
     */
    public function canUserDoRoleOption($optionName) {
        $roleAllowed = $this->getRoleOption($optionName);
        if ('anyone' == $roleAllowed) {
            return true;
        }
        return $this->isUserRoleEqualOrBetterThan($roleAllowed);
    }

    public function canUserAuthorizeVacation(){
        $default_allowed_editors = array('administrator');
        $allowed_editors = (get_option('can_do_authorize_vacation', $default_allowed_editors) ) ;
        $currentUserCan = false;

        foreach($allowed_editors as $allowed_editor){
            if (current_user_can($this->roleToCapability($allowed_editor))) {
                $currentUserCan = true;
                break;
            }
        }
        return $currentUserCan;
    }

    public function canUserRequestVacation(){
        $default_allowed_users = array('administrator', 'contributor', 'editor', 'author');
        $allowed_users = (get_option('em_can_do_request_vacation', $default_allowed_users) ) ;
        $currentUserCan = false;

        if(is_array($allowed_users)){
            foreach($allowed_users as $allowed_editor){
                if (current_user_can($this->roleToCapability($allowed_editor))) {
                    $currentUserCan = true;
                    break;
                }
            }
        }

        return $currentUserCan;
    }




    /**
     * Helper-function outputs the correct form element (input tag, select tag) for the given item
     * @param  $aOptionKey string name of the option (un-prefixed)
     * @param  $aOptionMeta mixed meta-data for $aOptionKey (either a string display-name or an array(display-name, option1, option2, ...)
     * @param  $savedOptionValue string current value for $aOptionKey
     * @return void
     */
    protected function createFormControl($aOptionKey, $aOptionMeta, $savedOptionValue) {

        $type = $aOptionMeta[1];

        switch($type){
            case "select":
                $choices = $aOptionMeta[2];
                ?>
                <p><select name="<?php echo $aOptionKey ?>" id="<?php echo $aOptionKey ?>">
                        <?php
                        foreach ($choices as $key => $aChoice) {
                            $selected = ($key == $savedOptionValue) ? 'selected' : '';
                            ?>
                            <option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $this->getOptionValueI18nString($aChoice) ?></option>
                        <?php
                        }
                        ?>
                    </select></p>
                <?php
                break;
            case "multiple":
                $choices = $aOptionMeta[2];
                ?>
                <p><select name="<?php echo $aOptionKey ?>[]" id="<?php echo $aOptionKey ?>" multiple>
                        <?php
                        foreach ($choices as $key => $aChoice) {
                            $selected = (in_array($key,$savedOptionValue)) ? 'selected' : '';
                            ?>
                            <option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $this->getOptionValueI18nString($aChoice) ?></option>
                        <?php
                        }
                        ?>
                </select></p>
            <?php
                break;
            case "text":
                ?>
                <p><input type="text" name="<?php echo $aOptionKey ?>" id="<?php echo $aOptionKey ?>"
                          value="<?php echo esc_attr($savedOptionValue) ?>" size="50"/></p>
                <?php
                break;
        }

    }

    /**
     * Override this method and follow its format.
     */
    protected function getOptionValueI18nString($optionValue) {
        return __($optionValue, 'employees-vacation-manager');
    }

    /**
     * Query MySQL DB for its version
     * @return string|false
     */
    protected function getMySqlVersion() {
        global $wpdb;
        $rows = $wpdb->get_results('select version() as mysqlversion');
        if (!empty($rows)) {
             return $rows[0]->mysqlversion;
        }
        return false;
    }

    /**
     * If you want to generate an email address like "no-reply@your-site.com" then
     * you can use this to get the domain name part.
     * E.g.  'no-reply@' . $this->getEmailDomain();
     * This code was stolen from the wp_mail function, where it generates a default
     * from "wordpress@your-site.com"
     * @return string domain name
     */
    public function getEmailDomain() {
        // Get the site domain and get rid of www.
        $sitename = strtolower($_SERVER['SERVER_NAME']);
        if (substr($sitename, 0, 4) == 'www.') {
            $sitename = substr($sitename, 4);
        }
        return $sitename;
    }


    /**
     * @param  $name string name of a database table
     * @return string input prefixed with the WordPress DB table prefix
     * plus the prefix for this plugin (lower-cased) to avoid table name collisions.
     * The plugin prefix is lower-cases as a best practice that all DB table names are lower case to
     * avoid issues on some platforms
     */
    protected function prefixTableName($name) {
        global $wpdb;
        return $wpdb->prefix .  strtolower($this->prefix($name));
    }

    public function saveAdminVacationRequest() {

        global $wpdb;
        $current_user = wp_get_current_user();
        $table_name = $this->prefixTableName('vacation_request');
        $date_format = ($date_format = $this->getOption('date_format'))? $date_format: 'DD.MM.YYYY';
        $php_date_format = $this->getPhpDateFomat();

        if (isset($_POST['admin-date-range']) && $_POST['admin-date-range'] != ""  && isset($_POST['admin-vacation-type'])
          && $_POST['admin-vacation-type']  != ""  && isset($_POST['admin-user']) && $_POST['admin-user'] != "" ){

            $dates = explode(' - ', $_POST['admin-date-range']);
            $status = (isset($_POST['admin-approve']) && $_POST['admin-approve'] == 1 )? 1 : 0;
            $comment = (isset($_POST['admin-vacation-comment']) && $_POST['admin-vacation-comment'] != "" )? $_POST['admin-vacation-comment']  : "";


            if(count($dates) == 2){
                $fromDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[0]));
                $toDate = DateTime::createFromFormat($php_date_format[$date_format], trim($dates[1]));

                $validation = $this->validateDates($_POST['admin-user'], $fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));

                if($validation){
                    if($this->checkUserAvailableVacationDays($dates, $_POST['admin-user'], $php_date_format[$date_format], $_POST['admin-vacation-type'])){
                        $now = current_time('mysql', false);
                        $history = array();
                        $history[] = array(current_time('mysql', false) => array('created' => $current_user->display_name));
                        if($status == 1)
                             $history[] = array(current_time('mysql', false) => array('approved' => $current_user->display_name));

                        $wpdb->insert($table_name, array("user" => $_POST['admin-user'], "time" => $now, "from_date" => date('Y-m-d', strtotime($fromDate->format('Y-m-d'))),
                                                         "to_date" => date('Y-m-d', strtotime($toDate->format('Y-m-d'))), "comment" => $comment,
                                                         "status" => $status, "type" => $_POST['admin-vacation-type'], "history" => serialize($history)));

                        if($_POST['admin-vacation-type'] == 'paid-vacation'){
                            $request_id = $wpdb->insert_id;
                            $this->saveVacationRequestDates($request_id, $fromDate, $toDate, $_POST['admin-user']);
                        }

                        add_filter( 'admin-employees-vacation-manager-notification', function(){
                            return array("type" => "success", "text" => __('The changes have been saved successfully', "employees-vacation-manager"));
                        });
                    }
                }
            }
        }
    }

    /**
     * @param int $request_id
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @param int $user
     */
    protected  function saveVacationRequestDates($request_id, $fromDate, $toDate, $user){

        global $wpdb;
        $request_dates_table_name = $this->prefixTableName('vacation_request_dates');
        $fromYear =  date('Y', strtotime($fromDate->format('Y-m-d')));
        $toYear =  date('Y', strtotime($toDate->format('Y-m-d')));



        if($fromYear != $toYear){
            $fromTempDate =  DateTime::createFromFormat('Y-m-d', $toYear.'-01-01');
            $toTempDate =  DateTime::createFromFormat('Y-m-d', $fromYear.'-12-31');
            $requestedDaysFirstDate = $fromDate->diff($toTempDate)->format('%a');
            $requestedDaysLastDate = $fromTempDate->diff($toDate)->format('%a');

            $workingDaysFirstDate = EmployeesManager_Utils::getWorkingDays($fromDate->format('Y-m-d'), $toTempDate->format('Y-m-d'));
            $workingDaysLastDate = EmployeesManager_Utils::getWorkingDays($fromTempDate->format('Y-m-d'), $toDate->format('Y-m-d'));

            $wpdb->insert($request_dates_table_name, array("request_id" => $request_id,"user" => $user, "from_date" => date('Y-m-d', strtotime($fromDate->format('Y-m-d'))),
                                             "to_date" => date('Y-m-d', strtotime($toTempDate->format('Y-m-d'))), "year" => $fromYear,
                                             "days" => ((int)$requestedDaysFirstDate), 'working_days' => $workingDaysFirstDate));

            $wpdb->insert($request_dates_table_name, array("request_id" => $request_id,"user" => $user, "from_date" => date('Y-m-d', strtotime($fromTempDate->format('Y-m-d'))),
                                                           "to_date" => date('Y-m-d', strtotime($toDate->format('Y-m-d'))), "year" => $toYear,
                                                           "days" => ((int)$requestedDaysLastDate + 1), 'working_days' => $workingDaysLastDate));
        }else{
            $requestedDays = $fromDate->diff($toDate)->format('%a');
            $workingDays = EmployeesManager_Utils::getWorkingDays($fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));
            $wpdb->insert($request_dates_table_name, array("request_id" => $request_id,"user" => $user, "from_date" => date('Y-m-d', strtotime($fromDate->format('Y-m-d'))),
                                                           "to_date" => date('Y-m-d', strtotime($toDate->format('Y-m-d'))), "year" => $fromYear,
                                                           "days" => ((int)$requestedDays), 'working_days' => $workingDays));
        }
    }

    /**
     * @param array $dates
     * @param int $userId
     * @param string $dateFormat
     * @param string $vacationType
     * @return bool
     */
    protected function checkUserAvailableVacationDays($dates, $userId, $dateFormat, $vacationType){

        global $wpdb;
        $request_dates_table_name = $this->prefixTableName('vacation_request_dates');

        if($vacationType == 'paid-vacation'){
            $vacations_days = ($vacations_days = $this->getOption('available_paid_vacation'))? $vacations_days : 30;

            $fromDate = DateTime::createFromFormat($dateFormat, trim($dates[0]));
            $toDate = DateTime::createFromFormat($dateFormat, trim($dates[1]));

            $fromYear =  date('Y', strtotime($fromDate->format('Y-m-d')));
            $toYear =  date('Y', strtotime($toDate->format('Y-m-d')));

            if($fromYear != $toYear){
                $fromTempDate =  DateTime::createFromFormat('Y-m-d', $toYear.'-01-01');
                $toTempDate =  DateTime::createFromFormat('Y-m-d', $fromYear.'-12-31');
                $requestedDaysFirstDate = $fromDate->diff($toTempDate)->format('%a');
                $requestedDaysLastDate = $fromTempDate->diff($toDate)->format('%a');

                $total_first_year = $wpdb->get_var( "SELECT sum(working_days) FROM ".$request_dates_table_name." WHERE user=".$userId." AND year =".$fromYear);
                $total_second_year = $wpdb->get_var( "SELECT sum(working_days) FROM ".$request_dates_table_name." WHERE user=".$userId." AND year =".$toYear );

                if((int)$requestedDaysFirstDate + $total_first_year > $vacations_days || (int)$requestedDaysLastDate + $total_second_year > $vacations_days ){
                    return false;
                }

            }else{
                $interval = $fromDate->diff($toDate);
                $requestedDays = $interval->format('%a');
                $total_first_year = $wpdb->get_var( "SELECT sum(working_days) FROM ".$request_dates_table_name." WHERE user=".$userId." AND year =".$fromYear);
                if((int)$requestedDays + $total_first_year > $vacations_days ){
                    return false;
                }
            }
        }

        return true;
    }



    /**
     * @param int $user
     * @param string $fromDate
     * @param string $toDate
     * @return bool
     */
    protected function validateDates($user, $fromDate, $toDate){

        global $wpdb;
        $table_name = $this->prefixTableName('vacation_request_dates');

        $fromYear =  date('Y', strtotime($fromDate));
        $fromMonth =  date('m', strtotime($fromDate));
        $fromDay =  date('d', strtotime($fromDate));

        $toYear =  date('Y', strtotime($toDate));
        $toMonth =  date('m', strtotime($toDate));
        $toDay =  date('d', strtotime($toDate));
        $current_year = date('Y');

        if(!checkdate($fromMonth, $fromDay, $fromYear) || !checkdate($toMonth, $toDay, $toYear)){
            add_filter( 'admin-employees-vacation-manager-notification', function(){
                return array("type" => "err", "text" => __('Invalid Date. Please enter a valid date.', "employees-vacation-manager"));
            });
            return false;
        }

        if($fromYear < $current_year - 1 || $fromYear > $current_year + 1 || $toYear < $current_year - 1 || $toYear > $current_year + 1 ){
            add_filter( 'admin-employees-vacation-manager-notification', function(){
                return array("type" => "err", "text" => __('Only applications with one difference year to current year  are accepted.', "employees-vacation-manager"));
            });
            return false;
        }

        $firstTry = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE user=".$user." AND from_date <= '".$fromDate."' AND to_date >= '".$fromDate. "'" );
        $twoTry = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE user=".$user." AND from_date <= '".$toDate."' AND to_date >= '".$toDate. "'" );

        add_filter( 'admin-employees-vacation-manager-notification', function(){
            return array("type" => "err", "text" => __('There are conflicts with other requests.', "employees-vacation-manager"));
        });

        return (empty($firstTry) && empty($twoTry))? true : false;

    }

    protected function getFilterLinks($current = "all"){

        global $wpdb;
        $table_name = $this->prefixTableName('vacation_request');

        $count['all'] =  $wpdb->get_var( 'SELECT COUNT(*) FROM '.$table_name );
        $count['in-progress'] = $wpdb->get_var( 'SELECT COUNT(*) FROM '.$table_name.'  WHERE status=0' );
        $count['approved'] =  $wpdb->get_var( 'SELECT COUNT(*) FROM '.$table_name.' WHERE status=1' );
        $count['rejected'] =  $wpdb->get_var( 'SELECT COUNT(*) FROM '.$table_name.'  WHERE status=2' );

        ?>

        <ul class="subsubsub">
            <li class="all">
                <a <?php if($current == "all"):?> class="current" <?php endif ?> href="<?php echo admin_url('admin.php?page=employees-manager-vacation-requests'); ?>">
                    All <span class="count">(<?php echo $count['all'] ?>)</span>
                </a> |
            </li>
            <li class="in-progress">
                <a  <?php if($current == "in-progress"):?> class="current" <?php endif ?> href="<?php echo add_query_arg('status', '0', admin_url('admin.php?page=employees-manager-vacation-requests')); ?>">
                    <?php _e('In Progress', 'employees-vacation-manager')?><span class="count">(<?php echo $count['in-progress'] ?>)</span>
                </a> |
            </li>
            <li class="approved">
                <a  <?php if($current == "approved"):?> class="current" <?php endif ?> href="<?php echo add_query_arg('status', '1', admin_url('admin.php?page=employees-manager-vacation-requests')); ?>">
                    <?php _e('Approved', 'employees-vacation-manager')?> <span class="count">(<?php echo $count['approved'] ?>)</span>
                </a> |
            </li>
            <li class="rejected">
                <a  <?php if($current == "rejected"):?> class="current" <?php endif ?> href="<?php echo add_query_arg('status', '2', admin_url('admin.php?page=employees-manager-vacation-requests')); ?>">
                    <?php _e('Rejected', 'employees-vacation-manager')?> <span class="count">(<?php echo $count['rejected'] ?>)</span>
                </a> |
            </li>
        </ul>
        <?php
    }




}

