<?php
/**
 * Employees Vacation Manager Extension
 *
 * @category     Extension
 * @copyright    Copyright © 2016 Aries Prodesign (http://ariesprodesign.de/)
 * @author       Aries Prodesign. Eduardo Garces Hernandez
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version     Release: 1.0.0
 */
include_once('EmployeesManager_AdminPages.php');

class EmployeesManager_LifeCycle extends EmployeesManager_AdminPages {

    public function install() {

        // Initialize Plugin Options
        $this->initOptions();

        // Initialize DB Tables used by the plugin
        $this->installDatabaseTables();

        // Other Plugin initialization - for the plugin writer to override as needed
        $this->otherInstall();

        // Record the installed version
        $this->saveInstalledVersion();

        // To avoid running install() more then once
        $this->markAsInstalled();
    }

    public function uninstall() {
        $this->otherUninstall();
        $this->unInstallDatabaseTables();
        $this->deleteSavedOptions();
        $this->markAsUnInstalled();
    }

    /**
     * Perform any version-upgrade activities prior to activation (e.g. database changes)
     * @return void
     */
    public function upgrade() {
        // Initialize DB Tables used by the plugin
        $this->installDatabaseTables();
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=105
     * @return void
     */
    public function activate() {
        $this->installDatabaseTables();
        $this->createPages();
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=105
     * @return void
     */
    public function deactivate() {
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return void
     */
    protected function initOptions() {
    }

    public function addActionsAndFilters() {
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
    }

    /**
     * Override to add any additional actions to be done at install time
     * See: http://plugin.michael-simpson.com/?page_id=33
     * @return void
     */
    protected function otherInstall() {
    }

    /**
     * Override to add any additional actions to be done at uninstall time
     * See: http://plugin.michael-simpson.com/?page_id=33
     * @return void
     */
    protected function otherUninstall() {
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function createPages() {
    }

    /**
     * Puts the configuration page in the Plugins menu by default.
     * Override to put it elsewhere or create a set of submenus
     * Override with an empty implementation if you don't want a configuration page
     * @return void
     */
    public function addSettingsSubMenuPage() {
        $this->addSettingsSubMenuPageToPluginsMenu();
        //$this->addSettingsSubMenuPageToSettingsMenu();
    }


    protected function requireExtraPluginFiles() {
        require_once(ABSPATH . 'wp-includes/pluggable.php');
        require_once(ABSPATH . 'wp-admin/includes/plugin.php');
    }

    /**
     * @return string Slug name for the URL to the Setting page
     * (i.e. the page for setting options)
     */
    protected function getSettingsSlug() {
        return get_class($this) . 'Settings';
    }

    protected function addSettingsSubMenuPageToPluginsMenu() {
        $this->requireExtraPluginFiles();
        $displayName = $this->getPluginDisplayName();

        add_menu_page( __($displayName, 'employees-vacation-manager'),
                       __($displayName, 'employees-vacation-manager'),
                       'manage_options',
                       'employees-vacation-manager',
                       array(&$this, 'vacationsOverviewPage'),
                       plugins_url( 'employees-vacation-manager/assets/images/icon.gif' )
        );

        add_submenu_page('employees-vacation-manager',
                         __('Employees Overview', 'employees-vacation-manager'),
                         __('Employees Overview', 'employees-vacation-manager'),
                         'manage_options',
                         'employees-manager-employees',
                         array(&$this, 'employeesOverviewPage')
        );

        add_submenu_page('employees-vacation-manager',
                         __('Calender Overview', 'employees-vacation-manager'),
                         __('Calender Overview', 'employees-vacation-manager'),
                         'manage_options',
                         'employees-manager-calender',
                         array(&$this, 'calenderOverviewPage')
        );

        add_submenu_page('employees-vacation-manager',
                         __('Holidays Overview', 'employees-vacation-manager'),
                         __('Holidays Overview', 'employees-vacation-manager'),
                         'manage_options',
                         'employees-manager-holidays',
                         array(&$this, 'holidaysOverviewPage')
        );

        add_submenu_page('employees-vacation-manager',
                         __('Settings', 'employees-vacation-manager'),
                         __('Settings', 'employees-vacation-manager'),
                         'manage_options',
                         'employees-manager-settings',
                         array(&$this, 'settingsPage')
        );
    }



    /**
     * Convenience function for creating AJAX URLs.
     *
     * @param $actionName string the name of the ajax action registered in a call like
     * add_action('wp_ajax_actionName', array(&$this, 'functionName'));
     *     and/or
     * add_action('wp_ajax_nopriv_actionName', array(&$this, 'functionName'));
     *
     * If have an additional parameters to add to the Ajax call, e.g. an "id" parameter,
     * you could call this function and append to the returned string like:
     *    $url = $this->getAjaxUrl('myaction&id=') . urlencode($id);
     * or more complex:
     *    $url = sprintf($this->getAjaxUrl('myaction&id=%s&var2=%s&var3=%s'), urlencode($id), urlencode($var2), urlencode($var3));
     *
     * @return string URL that can be used in a web page to make an Ajax call to $this->functionName
     */
    public function getAjaxUrl($actionName) {
        return admin_url('admin-ajax.php') . '?action=' . $actionName;
    }


    protected function parseArrayToObject($array) {
        $object = new stdClass();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $name=>$value) {
                $name = strtolower(trim($name));
                if (!empty($name)) {
                    $object->$name = $value;
                }
            }
        }
        return $object;
    }


}
