��    P      �  k         �     �     �     �     �     �             /        J     W  0   `     �  	   �     �     �     �     �  f   �     D  
   c     n     s          �     �  	   �     �     �     �     �     �     �  	   	     	     	     %	     7	  '   C	  	   k	  )   u	  I   �	     �	     �	     �	     
     *
     :
  	   C
     M
     Z
     g
     p
  
   w
  	   �
  (   �
  (   �
     �
     �
     �
     �
          %     7     Q     a     |     �  $   �     �  (   �  .        >  (   ]     �     �     �     �     �     �    �     �     �     �     �     �            ;   %     a     p  3   y     �  	   �  !   �  
   �  
   �  
   �  �      &   �  	   �     �     �     �     �     �  
   �     �     �          %  *   =  .   h     �     �     �     �     �  5   �       ?     P   M     �  
   �  $   �     �     �  	                  $     5     E     L     T  )   \  3   �     �     �  
   �     �     �       '   *     R  '   g  *   �     �  "   �  $   �  3     7   I     �  3   �     �     �     �     �  	   �                C          5   A       "   K       P   6   )   <                      
   J   3       G       &          -      !      %         ?       .      D              E            0          *   $       8   M   2       	   L      4          +   F           =   >   :   N              9   B   ,             @              H      #      /             '   1         7   O       I           (   ;        by  Action Active Add a new holiday Add a new request Add holiday Administrator Amount of available paid vacation days per year Apply Filter Approved Are you sure that you want to remove this entry? Author Authorize Back to the overview Calender Overview Comment Contributor Create a new page and insert the shortcode [ShowVacationCalender] to show the calender on the frontend Created a new vacation request Created on Date Date format Days Decline Delete Details:  Editor Employees Manager Employees Overview Employees Vacation Employees Vacation Manager Enter user page title From Date Go to History Holidays Overview In Progress Invalid Date. Please enter a valid date Mandatory Maximum available vacation days exceeded. Only applications with one difference year to current year  are accepted. Page  Paid vacation Planned vacations for the year Please choose Plugin Settings Rejected Returning Save Changes Send request Settings Status Subscriber Take over The changes have been saved successfully There are conflicts with other requests. To Date Unpaid vacation User Vacation Request Vacation Request Details Vacation Requests Vacation end (Date range) Vacation period Vacation requests Overview Vacation start (Date range) Vacation type Who can authorize the vacation days? Who can request vacations? You have received a new vacation request You have received a new vacation request form  Your request for vacation was  amount of available paid vacation a year approved created en permission denied rejected to Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Hernandez <info@ariesprodeisgn.de>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
Language: es
X-Generator: Poedit 1.8.7
  por  Acción Activo agregar un día festivo Crear nueva solicitud agregar día festivo Administrador Cantidad de días disponibles de vacaciones pagadas al año Aplicar filtro Aprobada ¿Está seguro que desea eliminar esta solicitud?   Autor Autorizar Volver al la lista de solicitudes Calendario Comentario Trabajador Crear una nueva página e insertar el código abreviado (shortcode) [ShowVacationCalender] para mostrar el calendario en el frontend Crea una nueva solicitud de vacaciones Creado el Fecha Formato de fecha Días Rechazar Eliminar Detalles:  Editor Administrador de personal Resumen de personal vacaciones de empleados Administrador de vacaciones para empleados Introduzca el título de la página de usuario Desde Ir a Historia Días festivos En procesamiento Fecha invalida. Por favor introduzca una fecha valida obligatorio excedido el número máximo de días de vacaciones disponibles. Sólo son aceptadas las solicitudes con un año de diferencia al año corriente. Pagina  Vacaciones Vacaciones planificadas para el año Seleccione por favor Plugin Configuración Rechazada Cancelar Guardar cambios Enviar solicitud Configuraciones Estado Abonado Aplicar Los cambios han sido guardados con éxito Hay conflictos con otras solicitudes de vacaciones. Hasta Vacaciones sin Pago Trabajador Solicitud de vacaciones Detalles de la Solicitud Solicitudes de vacaciones Fin de las vacaciones (rango de fechas) Tiempo de vacaciones Resumen de las peticiones de vacaciones Inicio de las vacaciones (rango de fechas) Tipo de vacaciones ¿Quién puede aprobar vacaciones? ¿Quién puede solicitar vacaciones? Usted ha recibido una nueva solicitud de vacaciones Usted ha recibido una nueva solicitud de vacaciones de  Su solicitud de vacaciones fue  Cantidad de días disponibles de vacaciones al año aprovada creada es Permiso denegado rechazada hasta 