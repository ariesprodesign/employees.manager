��    Q      �  m   ,      �     �     �     �     �               $  /   2     b     o  0   x     �  	   �     �     �     �     �  f   �     \  
   {     �     �     �     �     �  	   �     �     �     �     �     �     	  	   %	     /	     5	     =	     O	  '   [	  	   �	  )   �	  I   �	     
     
     
     4
     B
     R
  	   [
     e
     r
     
     �
  
   �
  	   �
  (   �
  (   �
     �
     �
               /     @     Y     k     �     �     �     �  $   �     �  (     .   C     r  (   �     �     �     �     �     �     �  	  �     �     �  	                   6     I  6   W     �  	   �  <   �     �  
   �     �       	        #  �   /  #   �     �     �     �     �     �  	     	     	              4     K     ^  !   y     �     �     �  	   �     �  9   �       /     W   D     �     �  "   �     �     �  	   �  
   �               )     7     >     G  .   S  .   �     �     �     �  '   �     �          #     3     O     [     v  
   �     �     �  ,   �  ;        @  6   [  	   �     �     �     �  	   �     �             C         B         D      2              &   P               %   H   8           -                3   /   L   I   :   7   A   F   )   Q   *       6   O   +       5                 ;   .   (   >   '   #      <                  
   ,      $   =   0   9       M   N       4                 ?   	               K       J          1         G      E          @   !   "                                  by  Action Active Add a new holiday Add a new request Add holiday Administrator Amount of available paid vacation days per year Apply Filter Approved Are you sure that you want to remove this entry? Author Authorize Back to the overview Calender Overview Comment Contributor Create a new page and insert the shortcode [ShowVacationCalender] to show the calender on the frontend Created a new vacation request Created on Date Date format Days Decline Delete Details:  Editor Employees Manager Employees Overview Employees Vacation Employees Vacation Manager Enter user page title From Date Go to History Holidays Overview In Progress Invalid Date. Please enter a valid date Mandatory Maximum available vacation days exceeded. Only applications with one difference year to current year  are accepted. Page  Paid vacation Planned vacations for the year Please choose Plugin Settings Rejected Returning Save Changes Send request Settings Status Subscriber Take over The changes have been saved successfully There are conflicts with other requests. To Date Unpaid vacation User Users can request vacation? Vacation Request Vacation Request Details Vacation Requests Vacation end (Date range) Vacation period Vacation requests Overview Vacation start (Date range) Vacation type Who can authorize the vacation days? Who can request vacations? You have received a new vacation request You have received a new vacation request form  Your request for vacation was  amount of available paid vacation a year approved created en permission denied rejected to Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Hernandez <info@ariesprodeisgn.de>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
Language: de_DE
X-Generator: Poedit 1.8.7
  von  Aktion Aktiviert Feiertag einfügen Neue Antrag erstellen Feiertag einfügen Administrator Menge der verfügbaren bezahlten Urlaubstagen pro Jahr Filtern Genehmigt Sind Sie sicher, dass Sie diesen Eintrag entfernen möchten? Autor Genehmigen Zurück zur Übersicht Kalender-Übersicht Kommentar Mitarbeiter Erstellen Sie eine neue Seite und fügen Sie den Shortcode [ShowVacationCalender] ein, um den Kalander auf dem Frontend zu zeigen einen neuen Urlaubsantrag Erstellen Erstellt am Datum Datumsformat Tage Ablehnen Entfernen Details:  Redakteur Mitarbeiter Manager Mitarbeiter Übersicht Mitarbeiter Urlaub Mitarbeiter Urlaub Manager Geben Sie den Benutzerseitentitel Von dem Gehe zu Verlauft Feiertage in Bearbeitung Ungültiges Datum. Bitte geben Sie ein gültiges Datum an Pflichfelder Maximal verfügbare Urlaubstage überschritten. Nur Anfragen mit einem Unterschied von einem Jahr zum laufenden Jahr werden akzeptiert. Seite  Urlaub Geplante Urlaubstage für das Jahr Bitte wählen Plugin Einstellungen Abgelehnt Stornieren Änderungen speichern Antrag senden Einstellungen Status Abonnent Übernehmen Die Änderungen wurden erfolgreich gespeichert Es gibt Konflikte mit anderen Urlaubsanträge. bis zum Unbezahlte Urlaub Mitarbeiter dürfen Mitarbeitern Urlaub beantragen? Urlaubsantrag Urlaubsantrag Details Urlaubsanträge Urlaubsende (Datumsbereich) Urlaubszeit Urlaubsanträge Übersicht Urlaubsbeginn (Datumsbereich) Urlaubsart Wer kann Urlaub genehmigen? Wer kann Urlaub beantragen? Sie haben einen neuen Urlaubsantrag erhalten Sie haben einen neuen Urlaubsantrag erhalten. Mitarbeiter:  Ihre Urlaubsanfrage wurde  Menge der verfügbaren bezahlten Urlaubstagen pro Jahr genehmigt Erstellt de Erlaubnis verweigert abgelehnt bis 