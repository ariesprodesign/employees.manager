msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Hernandez <info@ariesprodeisgn.de>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"X-Generator: Poedit 1.8.4\n"

msgid "en"
msgstr ""

msgid "Vacation Requests"
msgstr ""

msgid "Vacation Request"
msgstr ""

msgid "Take over"
msgstr ""

msgid "Authorize"
msgstr ""

msgid "Returning"
msgstr ""

msgid "Delete"
msgstr ""

msgid "Decline"
msgstr ""

msgid "Created on"
msgstr ""

msgid "User"
msgstr ""

msgid "From Date"
msgstr ""

msgid "To Date"
msgstr ""

msgid "Status"
msgstr ""

msgid "Vacation requests Overview"
msgstr ""

msgid "Approved"
msgstr ""

msgid "Rejected"
msgstr ""

msgid "Rejected"
msgstr ""

msgid "In Progress"
msgstr ""

msgid "Plugin Settings"
msgstr ""

msgid "Employees Manager"
msgstr ""

msgid "Employees Vacation Manager"
msgstr ""

msgid "Employees Vacation"
msgstr ""

msgid "Settings"
msgstr ""

msgid "Enter user page title"
msgstr ""

msgid "Amount of available paid vacation days per year"
msgstr ""

msgid "Users can request vacation?"
msgstr ""

msgid "Who can authorize the vacation days?"
msgstr ""

msgid "Save Changes"
msgstr ""

msgid "Add a new request"
msgstr ""

msgid "Active"
msgstr ""

msgid "to"
msgstr ""

msgid "Vacation period"
msgstr ""

msgid "Vacation type"
msgstr ""

msgid "Send request"
msgstr ""

msgid "Paid vacation"
msgstr ""

msgid "Unpaid vacation"
msgstr ""

msgid "Please choose"
msgstr ""

msgid "permission denied"
msgstr ""

msgid "The changes have been saved successfully"
msgstr ""

msgid "Days"
msgstr ""

msgid "Date format"
msgstr ""

msgid "Maximum available vacation days exceeded."
msgstr ""

msgid "There are conflicts with other requests."
msgstr ""

msgid "Are you sure that you want to remove this entry?"
msgstr ""

msgid "Created a new vacation request"
msgstr ""

msgid "Action"
msgstr ""

msgid "Planned vacations for the year"
msgstr ""

msgid "Employees Overview"
msgstr ""

msgid "Contributor"
msgstr ""

msgid "Subscriber"
msgstr ""

msgid "Author"
msgstr ""

msgid "Editor"
msgstr ""

msgid "Administrator"
msgstr ""

msgid "Apply Filter"
msgstr ""

msgid "Vacation start (Date range)"
msgstr ""

msgid "Vacation end (Date range)"
msgstr ""

msgid "Comment"
msgstr ""

msgid "Back to the overview"
msgstr ""

msgid "History"
msgstr ""

msgid "created"
msgstr ""

msgid "rejected"
msgstr ""

msgid "approved"
msgstr ""

msgid " by "
msgstr ""

msgid "Vacation Request Details"
msgstr ""

msgid "You have received a new vacation request"
msgstr ""

msgid "You have received a new vacation request form "
msgstr ""

msgid "Details: "
msgstr ""

msgid "Your request for vacation was "
msgstr ""

msgid "Only applications with one difference year to current year are accepted."
msgstr ""

msgid "Who can request vacations?"
msgstr ""

msgid "Page "
msgstr ""

msgid "Go to"
msgstr ""

msgid "Mandatory"
msgstr ""

msgid "Calender Overview"
msgstr ""

msgid "Create a new page and insert the shortcode [ShowVacationCalender] to show the calender on the frontend"
msgstr ""

msgid "Holidays Overview"
msgstr ""

msgid "Add a new holiday"
msgstr ""

msgid "Add holiday"
msgstr ""

msgid "Date"
msgstr ""

msgid "amount of available paid vacation a year"
msgstr ""

msgid "Invalid Date. Please enter a valid date"
msgstr ""
