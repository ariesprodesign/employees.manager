<?php
/**
 * Employees Vacation Manager Extension
 *
 * @category     Extension
 * @copyright    Copyright © 2016 Aries Prodesign (http://ariesprodesign.de/)
 * @author       Aries Prodesign. Eduardo Garces Hernandez
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version     Release: 1.0.0
 */
class EmployeesManager_Utils {

    /**
     *
     *
     * @param string $action
     * @param string $string
     * @param string $secret_key
     *
     * @return string
     */
    public static function encrypt_decrypt($action, $string, $secret_key = '!$%&we=') {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_iv = '"§$!=pu(';

        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt($string, $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public static function enqueue_frontend_scripts_and_styles(){
        add_action( 'wp_enqueue_scripts', function(){self::enqueue_scripts_and_styles();});
    }

    public static function enqueue_scripts_and_styles(){
            wp_enqueue_style(
                'daterangepicker-style',
                plugins_url() . '/employees-vacation-manager/assets/css/daterangepicker.css'
            );

            wp_enqueue_script(
                'moment-script',
                plugins_url() . '/employees-vacation-manager/assets/js/moment.min.js'
            );
            wp_enqueue_script(
                'daterangepicker-script',
                plugins_url() . '/employees-vacation-manager/assets/js/jquery.daterangepicker.js'
            );
            if ( ! is_admin() ) {
                wp_enqueue_style(
                    'tablesaw-style',
                    plugins_url() . '/employees-vacation-manager/assets/css/tablesaw.css'
                );

                wp_enqueue_script(
                    'tablesaw-script',
                    plugins_url() . '/employees-vacation-manager/assets/js/tablesaw.js'
                );
                wp_enqueue_script(
                    'tablesaw-init-script',
                    plugins_url() . '/employees-vacation-manager/assets/js/tablesaw-init.js'
                );
            }

            wp_enqueue_style(
                'fancybox-style',
                plugins_url() . '/employees-vacation-manager/lib/fancybox/jquery.fancybox.css'
            );
            wp_enqueue_script(
                'fancybox-script',
                plugins_url() . '/employees-vacation-manager/lib/fancybox/jquery.fancybox.js'
            );
            wp_enqueue_style(
                'employees-vacation-manager-style',
                plugins_url() . '/employees-vacation-manager/assets/css/employees-vacation-manager.css'
            );
    }

    public static function enqueue_calender_scripts_and_styles(){

        wp_enqueue_style(
            'fullcalender-style',
            plugins_url() . '/employees-vacation-manager/lib/fullcalendar/fullcalendar.min.css'
        );

        wp_enqueue_script(
            'moment-script',
            plugins_url() . '/employees-vacation-manager/assets/js/moment.min.js',
            array('jquery'), false, false
        );

        wp_enqueue_script(
            'fullcalender-script',
            plugins_url() . '/employees-vacation-manager/lib/fullcalendar/fullcalendar.min.js',
            array('jquery'), false, false
        );

        wp_enqueue_script(
            'fullcalender-script',
            plugins_url() . '/employees-vacation-manager/lib/fullcalendar/lang/'.__('en', 'employees-vacation-manager').'.js',
            array('fullcalender-script'), false, false
        );

        wp_enqueue_style(
            'employees-vacation-manager-style',
            plugins_url() . '/employees-vacation-manager/assets/css/employees-vacation-manager.css'
        );
    }

    public static function enqueue_holidays_scripts_and_styles(){

        wp_enqueue_script(
            'field-date-js',
            'Field_Date.js',
            array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'),
            time(),
            true
        );

        wp_enqueue_style( 'jquery-ui-datepicker' );
    }

    public static function count_workdays($date1,$date2){
        $firstDate = strtotime($date1);
        $lastDate = strtotime($date2);
        $firstDay = date('w',$firstDate);
        $lastDay = date('w',$lastDate);
        $totalDays = intval(($lastDate-$firstDate)/86400)+1;

        //check for one week only
        if ($totalDays<=7 && $firstDay<=$lastDay){
            $workdays = $lastDay-$firstDay+1;
            //check for weekend
            if ($firstDay==0){
                $workdays = $workdays-1;
            }
            if ($lastDay==6){
                $workdays = $workdays-1;
            }

        }else { //more than one week

            //workdays of first week
            if ($firstDay==0){
                //so we don't count weekend
                $firstWeek = 5;
            }else {
                $firstWeek = 6-$firstDay;
            }
            $totalFw = 7-$firstDay;

            //workdays of last week
            if ($lastDate==6){
                //so we don't count sat, sun=0 so it won't be counted anyway
                $lastWeek = 5;
            }else {
                $lastWeek = $lastDay;
            }
            $totalLw = $lastDay+1;

            //check for any mid-weeks
            if (($totalFw+$totalLw)>=$totalDays){
                $midweeks = 0;
            } else { //count midweeks
                $midweeks = (($totalDays-$totalFw-$totalLw)/7)*5;
            }

            //total num of workdays
            $workdays = $firstWeek+$midweeks+$lastWeek;

        }

        return ($workdays);
    }

    public static function getWorkingDays($startDate,$endDate){
        global $wpdb;

        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);

        $holidays_table_name = $wpdb->prefix."em_holidays_dates";
        $holidays = $wpdb->get_col( "SELECT date FROM ".$holidays_table_name );
        $holidays = ($holidays == NULL)?  array() : $holidays ;

        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {

            if ($the_first_day_of_week == 7) {

                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {

                    $no_remaining_days--;
                }
            }
            else {
                $no_remaining_days -= 2;
            }
        }

        $workingDays = $no_full_weeks * 5;

        if ($no_remaining_days > 0 )
        {
            $workingDays += $no_remaining_days;
        }

        foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);

            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }

        return round($workingDays);
    }
}

