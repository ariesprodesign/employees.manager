<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class EmployeesManager_UsersListTable extends WP_List_Table {

    public $items;

	/** Class constructor */
	public function __construct() {
        $users = get_users();
        $this->items = $this->fill_items($users);
		parent::__construct( [
			'singular' => __( 'Vacation requests Overview', 'employees-vacation-manager' ), //singular name of the listed records
			'plural'   => __( 'Vacation requests Overview', 'employees-vacation-manager' ), //plural name of the listed records
			'ajax'     => false, //does this table support ajax?
            'screen'   => 'employees-manager-employees'
		] );

	}



	/** Text displayed when no customer data is available */
	public function no_items() {
		_e( 'No Users available.', 'employees-vacation-manager' );
	}


	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {

        $current_year = date('Y');

		$columns = [
			'user'    => __( 'User', 'employees-vacation-manager' ),
            'total-first-year'    => __( 'Planned vacations for the year', 'employees-vacation-manager' ).' '. ($current_year - 1),
            'total-current-year' => __( 'Planned vacations for the year', 'employees-vacation-manager' ).' '. $current_year ,
            'total-last-year'    => __( 'Planned vacations for the year', 'employees-vacation-manager' ).' '. ($current_year + 1)
		];

		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(

			'user' => array( 'user', true ),
			'total-first-year' => array( 'total-first-year', true ),
            'total-current-year' => array( 'total-current-year', true ),
            'total-last-year' => array( 'total-last-year', true )
		);

		return $sortable_columns;
	}



	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		$per_page     = $this->get_items_per_page( 'users_per_page', 5 );
		$current_page = $this->get_pagenum();
		$total_items  = count($this->items);

        usort( $this->items, array( &$this, 'sort_data' ) );
        $data = $this->get_vacation_requests( $per_page, $current_page );

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );

		$this->items = $data;
	}



    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'user':
            case 'total-first-year':
            case 'total-current-year':
            case 'total-last-year':
                return $item[$column_name];
            default:
                return "" ;
        }
    }



    protected  function fill_items($items){
        global $wpdb;
        $dates_table_name = $wpdb->prefix."em_vacation_request_dates";
        $data = array();
        $current_year = date('Y');

        if (!empty($items)) {
            foreach ($items as $user) {
                $total_first_year = $wpdb->get_var( "SELECT sum(working_days) FROM ".$dates_table_name." WHERE user=".$user->ID." AND year =".($current_year - 1));
                $total_current_year = $wpdb->get_var( "SELECT sum(working_days) FROM ".$dates_table_name." WHERE user=".$user->ID." AND year =".$current_year );
                $total_second_year = $wpdb->get_var( "SELECT sum(working_days) FROM ".$dates_table_name." WHERE user=".$user->ID." AND year =".((int)$current_year + 1 ));
                $user_name = ($user->last_name != "")?$user->last_name . ', ' . $user->first_name : $user->display_name;

                $data[] = array(
                    'user'      => $user_name,
                    'total-first-year'    => ($total_first_year > 0)?$total_first_year : 0 ,
                    'total-current-year' => ($total_current_year > 0)?$total_current_year : 0 ,
                    'total-last-year'    => ($total_second_year > 0)?$total_second_year : 0
                );
            }
        }
        return $data;
    }

    protected  function get_vacation_requests($per_page, $current_page){

        if (!empty($this->items)) {

            $count = count($this->items);
            $first_item = ($current_page == 1) ? 0 : $per_page * ($current_page - 1);
            $pageItems = ($count > $per_page)? array_slice($this->items, $first_item, $per_page): $this->items;

            return $pageItems;
        }
        return $this->items;
    }


    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'user';
        $order = 'asc';
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }

        if(is_numeric($a[$orderby]) && is_numeric($b[$orderby])){
            $result = ( $a[$orderby] > $b[$orderby] ) ? 1 : -1;
        }else{
            $result = strcmp( $a[$orderby], $b[$orderby] );
        }

        if($order === 'asc')
        {
            return $result;
        }
        return -$result;
    }
}


