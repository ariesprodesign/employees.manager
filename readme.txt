=== Employees Vacation Manager ===
Contributors: Eduardo Garces Hernandez
Tags: Vacation, Employees Vacation Scheduler, Employees Vacation Manager
License: OSL 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 0.1

Manage your employees

== Description ==

How it works:

Using ShiftPlanning’s vacation request feature, the employee initiates the request:

From the Scheduling screen, your employee will be prompted to:
 - Specify the exact leave and return dates.

When the employee vacation request reaches management, you will receive a notification E-mail.

 - All “pending vacation” requests will be visible.
 - To approve or reject vacation time, you select the request and choose the action from the action drop-down menu.
 - Once approved or denied, the employee will receive an E-mail notification regarding the status of their vacation request.
 - Click the “Details” link to view any comments that the employee included with the vacation request and the history of this vacation request (created by, rejected by etc.).
 - Secure Forms With Form Keys.
 - Look at a calendar, all requests are approved.

Benefits:

 - Management receives notification of any request made.
 - Employees have more freedom to request vacation and leave time, and receive a decision in a timely manner.
 - Cumbersome and time-consuming paperwork is eliminated.
 - Data is kept in a centralized location reducing potential schedule conflicts.

Languages:
 - English
 - German
 - Spanish

Design:
 - Responsive
 - Intuitive

== Installation ==
 - Upload "Employees Vacation Manager" to the "/wp-content/plugins/" directory.
 - Activate the plugin through the Plugins menu item in the WordPress Dashboard.
 - The plugin will publish automatically a new "Vacation Request" page (If that do not already exist). You can change later the pages title.
 - Make sure, that the page "Vacation Request" is accessible from the navigation menu.
 - Done!

== Changelog ==

= 0.1 =
- Initial Revision
